*************************************************
Сервера для работы с оборудованием, базами данных 
*************************************************

.. automodule:: server.__init__
    :members:

Сервер авторизации
==================

.. automodule:: server.authorize.authorize
    :members:

Переброс данных из dbf в ORM
============================
.. automodule:: server.dbf.putev
    :members:

.. automodule:: server.dbf.readdbf
    :members:
    