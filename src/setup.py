# -*- coding: utf-8  -*-
from setuptools import setup, find_packages
import os
import logging, logging.config
if not os.path.exists('logs'):
    os.mkdir('logs')
# logging.config
logging.basicConfig(filename='logs/test.log',
                    filemode='w',
                    level=logging.DEBUG,
                    # format = '[%(asctime)s]# %(levelname)-8s %(threadName)-13s'
                    # ' %(name)-40s %(message)-100s [%(funcName)s:LINE:%(lineno)d]',
                    format = '[%(asctime)s]# %(levelname)-8s '
                    ' %(name)-40s %(message)-100s [%(funcName)s:LINE:%(lineno)d]',
                    )
log = logging.getLogger(__name__)



here = os.path.abspath(os.path.dirname(__file__))
with open(os.path.join(here, 'README.txt')) as f:
    README = f.read()
with open(os.path.join(here, 'CHANGES.txt')) as f:
    CHANGES = f.read()
import logsetup
requires = [
    'pyramid',
    'pyramid_chameleon',
    'pyramid_debugtoolbar',
    'waitress',
    'reportlab',
    'watchdog',
    'pathtools',
    'beautifulsoup4',
    ]

setup(name='asup',
      version='0.0',
      description='asup',
      long_description=README + '\n\n' + CHANGES,
      classifiers=[
        "Programming Language :: Python",
        "Framework :: Pyramid",
        "Topic :: Internet :: WWW/HTTP",
        "Topic :: Internet :: WWW/HTTP :: WSGI :: Application",
        ],
      author='',
      author_email='',
      url='',
      keywords='web pyramid pylons',
      packages=find_packages(),
      include_package_data=True,
      zip_safe=False,
      install_requires=requires,
      tests_require=requires,
      test_suite="asup",
      entry_points="""\
      [paste.app_factory]
      main = asup:main
      """,
      )
