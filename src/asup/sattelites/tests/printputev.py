# -*- coding: utf-8  -*-'''
import unittest
from pprint import pprint
import time, os, sys
import json
from pyramid import testing
from PyQt4 import QtGui
from ..printputev import Report, PrintReportSVG
import logging
import threading
log = logging.getLogger(__name__)


class PrintPutevTests(unittest.TestCase):
    # def setUp(self):
    #     self.config = testing.setUp()
    #
    # def tearDown(self):
    #     testing.tearDown()

    def test_watchfiles(self):
        from watchdog.observers import Observer
        from watchdog.events import LoggingEventHandler, FileSystemEventHandler

        # class Waiting(threading.Thread):
        #     def __init__(self, value):
        #         threading.Thread.__init__(self)
        #         self._value = value
        #         self._timesleep = 0.01
        #         self._timewait = 5
        #
        #     def run(self):
        #         t = time.time()
        #         while self._value:
        #             time.sleep(self._timesleep)
        #             if time.time() - t > self._timewait:
        #                 break

        class Handler(FileSystemEventHandler):
            def __init__(self, parent):
                FileSystemEventHandler.__init__(self)
                self._parent = parent
            def on_created(self, event):
                log.debug(event)
                self._parent.noeventrecv = False
            def on_deleted(self, event):
                log.debug(event)
                self._parent.noeventrecv = False
            def on_moved(self, event):
                log.debug(event)
                self._parent.noeventrecv = False
            def on_modified(self, event):
                log.debug(event)
                self._parent.noeventrecv = False

        self.noeventrecv = True
        fname = '123.txt'
        path = '.'
        event = LoggingEventHandler()
        observer = Observer(timeout=0.1)
        # observer.schedule(event, path, recursive=True)
        observer.schedule(Handler(self), path, recursive=False)
        observer.start()
        log.debug('file created')
        t = time.time()
        with open(path+'/'+fname, 'w') as f:
            f.write('2134')
        while self.noeventrecv and time.time() - t < 2:
            time.sleep(0.01)
        self.noeventrecv = True
        t = time.time()
        os.remove(path+'/'+fname)
        while self.noeventrecv and time.time() - t < 2:
            time.sleep(0.01)
        observer.stop()
        observer.join()

    def test_readfilejson(self):
        fname = 'tmp/JSON/PECPUT1.TXT.utf8'
        with open(fname, 'r') as f:
            s = f.read()
        d = json.loads(s)
        self.assertEqual(d['Graf66_4'], '2н')
        self.assertEqual(d['Graf27'], 19)
        self.assertEqual(d['Graf70_1'], '17.30')

    def test_readfilejson(self):
        fname = 'asup/sattelites/tests/data/PECPUT1.TXT.utf8'
        with open(fname, 'r') as f:
            s = f.read()
        d = json.loads(s)
        r = Report()
        r.fillvalue(d)
        # pprint(r._values, width=300)

        self.assertEqual(r._values['Graf66_4'], u'2н')
        self.assertEqual(r._values['Graf27'], 19)
        self.assertEqual(r._values['Graf70_1'], u'17.30')

    def test_print(self):
        app = QtGui.QApplication(sys.argv)
        printrep = PrintReportSVG()
        printrep.setprinter()
        rep = Report()
        fname = 'asup/sattelites/tests/data/PECPUT1.TXT.utf8'
        with open(fname, 'r') as f:
            s = f.read()
        rep.fillvalue(json.loads(s))


        printrep.data = []
        printrep.data += [rep.getsvgrender('asup/sattelites/tests/data/put1.svg')]
        printrep.data += [rep.getsvgrender('asup/sattelites/tests/data/put2.svg')]
        printrep.printpreview()
        #printrep.printing()
