# -*- coding: utf-8  -*-'''
import sys
import os
import json
# from reportlab.pdfgen.canvas import Canvas
# from reportlab.lib.pagesizes import A4
# from reportlab.pdfbase import pdfmetrics
# from reportlab.pdfbase.ttfonts import TTFont
# import threading
# from watchdog.observers import Observer
# from watchdog.events import LoggingEventHandler, FileSystemEventHandler, FileModifiedEvent
import re
from bs4 import BeautifulSoup
from PyQt4 import QtGui, QtCore, QtSvg
import locale
import logging

logging.basicConfig(filename='logs/test.log',
                    filemode='w',
                    level=logging.DEBUG,
                    # format = '[%(asctime)s]# %(levelname)-8s %(threadName)-13s'
                    # ' %(name)-40s %(message)-100s [%(funcName)s:LINE:%(lineno)d]',
                    format = '[%(asctime)s]# %(levelname)-8s '
                    ' %(name)-40s %(message)-100s [%(funcName)s:LINE:%(lineno)d]',
                    )


log = logging.getLogger(__name__)
enc = locale.getpreferredencoding()

class Report():
    ''' работа с шаблоном
    '''
    def __init__(self):
        self._fields = ['Stamp1', 'Text2', 'Avt', 'Gos', 'Gar', 'Famil', 'Udost', 'Tabel',
                        'Zakaz', 'Speed', 'Ostat', 'Komp1', 'Komp2', 'Komp3', 'Den', 'Mes',
                        'God', 'Nom']
        self._values = {}
        # генерируем возможные графы Graf0 Graf0_1 ... Graf99_9
        for i in range(100):
            for j in range(12):
                if j == 0:
                    self._fields += ['Graf{}'.format(i)]
                else:
                    self._fields += ['Graf{}_{}'.format(i, j)]
        self.clearvalue()

    def clearvalue(self):
        for i in self._fields:
            self._values[i] = ''

    def fillvalue(self, data):
        '''
        :param data: данные для заполнения значений
        '''
        for key in data.keys():
            if key in self._fields:
                self._values[key] = data[key]
            else:
                log.error('not found key [{}]'.format(key))
        return self._values

    def getsvgrender(self, filetemplate):
        '''Рендер шаблона на основае переданных ключей в data и имени файла filetemplate
        в щаблоне ищем тэги 'text' и по id подменяем на необходимые значения'''
        with open(filetemplate, "r") as f:
            logging.debug(('open file {0}'.format(f)).decode(enc))
            svg = f.read()
            soup = BeautifulSoup(svg)
            tags = soup.findAll("text")
            for tag in tags:
                if tag["id"] in self._values:
                    span = tag.tspan
                    spr = self._values[tag["id"]]
                    if type(spr) == int:
                        spr = str(spr)
                    span.string = spr
            return str(soup)

class PrintReportSVG(QtGui.QWidget):
    'Класс печати SVG файлов'
    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)
        self.data = None
        self._printer = None
        self._settings = {} #настройки принтера

    def setprinter(self, orientation = QtGui.QPrinter.Landscape,
                   fullpage = True, pagesize = QtGui.QPrinter.A4,
                   duplex = QtGui.QPrinter.DuplexShortSide):
        'Установка принтера'
        self._settings["orientation"] = orientation
        self._settings['fullpage'] = fullpage
        self._settings['pagesize'] = pagesize
        self._printer = QtGui.QPrinter()
        self._printer.setOrientation(orientation)
        self._printer.setFullPage(fullpage)
        self._printer.setPageSize(pagesize)
        self._printer.setDuplex(duplex)

    def printing(self, qprinter = None):
        '''Вывод на печать подменяя данные в шаблоне

            * filetemplate - имя файла шаблона
            * data  - список пар ключ и его значение в шаблоне'''
        printer = self._printer if qprinter is None else qprinter
        if printer is not None:
            logging.debug('send to printer [{}]'.format(printer))
            painter = QtGui.QPainter()
            painter.begin(printer)
            newpage = False
            for data in self.data:
                if newpage:
                    printer.newPage()
                render = QtSvg.QSvgRenderer(QtCore.QByteArray(data))
                render.render(painter)
                newpage = True
            painter.end()

    def printpreview(self, orientation = QtGui.QPrinter.Landscape,
                   fullpage = True, pagesize = QtGui.QPrinter.A4,
                   duplex = QtGui.QPrinter.DuplexShortSide):
        'Старт печати с предпросмотром'
        printer = QtGui.QPrinter()
        printer.setOrientation(orientation)
        printer.setFullPage(fullpage)
        printer.setPageSize(pagesize)
        printer.setDuplex(duplex)
        pp = QtGui.QPrintPreviewDialog(printer)
        pp.connect(pp, QtCore.SIGNAL('paintRequested(QPrinter*)'), self.printing)
        pp.exec_()

class MainWindow(QtGui.QMainWindow):
    def __init__(self, parent = None):
        QtGui.QMainWindow.__init__(self, parent)
        self._path = 'D:/PUT_VYP/JSON/'
        #self._path = 'report/'
        self.setWindowTitle(u'Настройки')
        self._setcentral()
        self._settray()
        self._setreport()
        self._setprinter()
        self._setfileobserver()
        self._timer = QtCore.QTimer(self)
        self.connect(self._timer, QtCore.SIGNAL('timeout()'), self._hidewindow)
        self._timer.start(10)
        self._re = re.compile(ur'"(\S*)": ("(.*)",|(\d*),)')
        self._tmpl = {
            1: ['static/tmpl/put1.svg', 'static/tmpl/put2.svg'],
            2: ['static/tmpl/put1_2.svg', 'static/tmpl/put2_2.svg'],
            4: ['static/tmpl/put1_4.svg',  'static/tmpl/put2_4.svg'],
            5: ['static/tmpl/put1_5.svg',  'static/tmpl/put2_5.svg'],
            6: ['static/tmpl/put1_6.svg', 'static/tmpl/put2_6.svg'],
            7: ['static/tmpl/put1_7.svg', 'static/tmpl/put2_7.svg'],
            9: ['static/tmpl/put1_9.svg',  'static/tmpl/put2_9.svg'],
            10: ['static/tmpl/put1_10.svg',  'static/tmpl/put2_10.svg'],
            11: ['static/tmpl/put1_11.svg',  'static/tmpl/put2_11.svg'],
            }


    def _setreport(self):
        self._report = Report()

    def _setprinter(self):
        self._printrep = PrintReportSVG()
        self._printrep.setprinter()

    def _setfileobserver(self):
        self._watcher = QtCore.QFileSystemWatcher()
        self._watcher.addPath(self._path)
        self.connect(self._watcher, QtCore.SIGNAL('directoryChanged(QString)'), self._handle_filechanged)

    def _handle_filechanged(self, path):
        # print ("_handle_filechanged {}".format(path))
        if len(os.listdir(path)):
            log.debug("{} {} ".format(len(os.listdir(path)), os.stat(path + os.listdir(path)[0]).st_size))

        if len(os.listdir(path)) and os.stat(path + os.listdir(path)[0]).st_size:
            fpath = str(path + os.listdir(path)[0])
            self._print_report(fpath)
            try:
                os.remove(fpath)
            except OSError as e:
                log.warning('{}'.format(e))

    def _print_report(self, filename):
        log.debug("_print_report {}".format(filename))
        f = os.path.split(filename)[1]
        fn = (f[6:]).split('.')[0]
        fn1 = int(fn[0])
        try:
            fn1 = int(fn[0:2])
        except:
            pass
        if fn1 in self._tmpl:
            tmpl1 = self._tmpl[fn1][0]
            tmpl2 = self._tmpl[fn1][1]
        else:
            return
        with open(filename, 'r') as f:
            data = f.read()
        data = data.decode('cp1251').encode('utf-8')
        res = re.findall(self._re, data)
        d = {}
        for i in res:
            if i[2]:
                d[i[0]] = i[2]
            else:
                d[i[0]] = i[3]

        data = d
        #data = json.loads(data)
        self._report.clearvalue()
        self._report.fillvalue(data)
        self._printrep.data = []
        self._printrep.data += [self._report.getsvgrender(tmpl1)]
        self._printrep.data += [self._report.getsvgrender(tmpl2)]
        #self._printrep.printpreview()
        self._printrep.printing()

    def _setcentral(self):
        screen = QtGui.QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move((screen.width()-size.width())/2,
            (screen.height()-size.height())/2)

    def _settray(self):
        trayiconmenu = QtGui.QMenu(self)
        action = QtGui.QAction(u'Настройки', self)
        self.connect(action, QtCore.SIGNAL('triggered()'), self._settings )
        trayiconmenu.addAction(action)
        trayicon = QtGui.QSystemTrayIcon(QtGui.QIcon('static/img/icon.png'), self)
        trayicon.setContextMenu(trayiconmenu)
        trayicon.show()

    def _settings(self):
        self.setVisible(True)

    def _hidewindow(self):
        self.setVisible(False)
        self._timer.stop()

def main():
    app = QtGui.QApplication(sys.argv)
    mw = MainWindow()
    mw.show()
    app.exec_()

if __name__ == "__main__":
    main()
