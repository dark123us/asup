# -*- coding: utf-8  -*-'''
from bs4 import BeautifulSoup
from PyQt4 import QtGui, QtCore, QtSvg
import logging
import locale
enc = locale.getpreferredencoding()


class PrintReport():
    'Класс печати отчетов'

class PrintReportSVG(QtGui.QWidget):
    'Класс печати SVG файлов'
    _filetemplate = '' #файл шаблона
    _data = {} #список пар ключ - значение, которое необходимо подменить
    _printer = None
    _settings = {} #настройки принтера
    
    def settemplate(self, filename):    
        self._filetemplate = filename
    
    def setdata(self, data):
        self._data = data 
        
    def getsvgrender(self):
        '''Рендер шаблона на основае переданных ключей в data и имени файла filetemplate
        в щаблоне ищем тэги 'text' и по id подменяем на необходимые значения'''
        with open(self._filetemplate, "r") as f:
            logging.debug(('open file {0}'.format(f)).decode(enc))
            svg = f.read()
            soup = BeautifulSoup(svg)
            tags = soup.findAll("text")
            for tag in tags:
                if tag["id"] in self._data.keys():
                    span = tag.tspan
                    spr = self._data[tag["id"]]
                    span.string = spr
            return str(soup)

    def setprinter(self, orientation = QtGui.QPrinter.Portrait, 
                   fullpage = True, pagesize = QtGui.QPrinter.A3):
        'Установка принтера'
        self._settings["orientation"] = orientation
        self._settings['fullpage'] = fullpage
        self._settings['pagesize'] = pagesize
        self._printer = QtGui.QPrinter()
        self._printer.setOrientation(orientation)
        self._printer.setFullPage(fullpage)
        self._printer.setPageSize(pagesize)
    
    def printing(self, qprinter = None):
        '''Вывод на печать подменяя данные в шаблоне
            
            * filetemplate - имя файла шаблона
            * data  - список пар ключ и его значение в шаблоне'''
        printer = self._printer
        if qprinter:
            printer = qprinter
        if printer:
            logging.debug('send to printer')
            painter = QtGui.QPainter()
            painter.begin(printer)
            bar = self.getsvgrender()
            render = QtSvg.QSvgRenderer(QtCore.QByteArray(bar))
            render.render(painter)
            painter.end()

    def printpreview(self, orientation = QtGui.QPrinter.Portrait,
                   fullpage = True, pagesize = QtGui.QPrinter.A3):
        'Старт печати с предпросмотром'
        printer = QtGui.QPrinter()
        printer.setOrientation(orientation)
        printer.setFullPage(fullpage)
        printer.setPageSize(pagesize)
        pp = QtGui.QPrintPreviewDialog(printer)
        pp.connect(pp, QtCore.SIGNAL('paintRequested(QPrinter*)'), self.printing)        
        pp.exec_()
    