#!/usr/bin/python
# -*- coding: UTF-8  -*-
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import (Column, Integer, String, Time, Date, 
                        Interval, ForeignKey, DateTime, Float, Boolean,
                        )
from sqlalchemy import engine_from_config
from sqlalchemy.orm import (
    scoped_session,
    sessionmaker,
    relationship,
    )

from zope.sqlalchemy import ZopeTransactionExtension
from datetime import date, datetime 

#DBSession = scoped_session(sessionmaker(autoflush=True))
DBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension(),
                                        expire_on_commit=False))
Base = declarative_base()

class Engine():
    _engine = None
    def __init__(self, settings):
        '''settings = {}:
            'sqlalchemy.url':'sqlite:///:memory:'
            'sqlalchemy.echo':True
            '''
        self._engine = engine_from_config(settings, 'sqlalchemy.')
    
    def configure(self):
        DBSession.remove()
        DBSession.configure(bind=self._engine)
        Base.metadata.bind = self._engine
    
    def createbase(self, tables = None):
        'tables – Optional list of Table objects, which is a subset of the total tables in the MetaData (others are ignored).'
        Base.metadata.create_all(bind = self._engine, tables = tables)

#//////////////////////////////////////////////////////////////////////////////////////
class Marka(Base):
    'Марка'
    __tablename__ = 'marka'
    id          = Column(Integer, primary_key=True)
    name        = Column(String(200))
    crmid       = Column(Integer)
    crmidosnov  = Column(Integer)
    name2       = Column(String(200))
    
    def __init__(self, name=''):
        self.name = name
#//////////////////////////////////////////////////////////////////////////////////////
class Bus(Base):
    'Автобус'    
    __tablename__ = 'bus'
    id        = Column(Integer, primary_key=True)
    garnm     = Column(Integer)     #гаражный номер
    fullgarnm = Column(Integer)     #полный номер
    gosnm     = Column(String(20))  #госномер
    markaid   = Column(Integer, ForeignKey(Marka.id))     #ссылка на марку автобуса
    marka     = relationship(Marka)
    dtvvoda   = Column(Date)    #дата ввода
    dtvyvoda  = Column(Date)    #дата вывода
    
    def __init__(self, garnm = 0, gosnm = '', markaid = 0):
        self.garnm = garnm
        self.gosnm = gosnm
        self.markaid = markaid
#//////////////////////////////////////////////////////////////////////////////////////    
class TypeService(Base):
    __tablename__ = 'typeservice'
    id          = Column(Integer, primary_key=True)
    name        = Column(String(20))
    fullname    = Column(String(100))
    'Тип обслуживания'
    def __init__(self, name, fullname, id = None):
        if id: 
            self.id = id
        self.name = name
        self.fullname = fullname

#//////////////////////////////////////////////////////////////////////////////////////         
class Service(Base):
    'Обслуживание'
    __tablename__ = 'service'
    id              = Column(Integer, primary_key=True)
    busid           = Column(Integer, ForeignKey(Bus.id))     
    bus             = relationship(Bus)
    typeserviceid   = Column(Integer, ForeignKey(TypeService.id))
    typeservice     = relationship(TypeService) #тип обслуживания

#//////////////////////////////////////////////////////////////////////////////////////    
class ServiceTo1To2(Base):
    'справочник ТО-1 ТО-2'
    __tablename__ = 'serviceto1to2'
    id = Column(Integer, primary_key=True)
    markaid = Column(Integer, ForeignKey('marka.id'))     
    marka   = relationship(Marka, order_by = Marka.name)
    to1km   = Column(Integer)
    to2km   = Column(Integer)
    
    def __init__(self, markaid = 0, km1 = 0 , km2 = 0):
        self.markaid = markaid
        self.to1km = km1
        self.to2km = km2
    
    def __repr__(self):
        s = "[%d]%s\t%d\t%d"%(self.markaid, self.marka.name, self.to1km, self.to2km)
        return s
#//////////////////////////////////////////////////////////////////////////////////////
class Probeg(Base):
    __tablename__ = 'probeg'
    id      = Column(Integer, primary_key=True)
    bus     = relationship(Bus)
    busid   = Column(Integer, ForeignKey(Bus.id))
    dt      = Column(Date)
    km      = Column(Float)

    def __init__(self, busid = 0, dt = date.today(), km = 0.0):
        self.busid = busid
        self.dt = dt
        self.km = km
#//////////////////////////////////////////////////////////////////////////////////////
class GroupUser(Base):
    'Группы пользователей системы'
    __tablename__ = 'groupuser'
    id        = Column(Integer, primary_key=True)
    name      = Column(String(50))
    dbengine  = Column(String(50))
    host      = Column(String(50))
    dbname    = Column(String(50))
    user      = Column(String(50))
    password  = Column(String(50))
    component = Column(Integer)
#//////////////////////////////////////////////////////////////////////////////////////
class User(Base):
    'Пользователи системы'
    __tablename__ = 'user'
    id        = Column(Integer, primary_key=True)
    name      = Column(String(50))
    password  = Column(String(50))
    group     = relationship(GroupUser)
    groupid   = Column(Integer, ForeignKey(GroupUser.id))
#//////////////////////////////////////////////////////////////////////////////////////
class DocumentServiceTo1To2(Base):
    'Документ назначения дня прохождения ТО-1 ТО-2'
    __tablename__ = 'docserviceto1to2'
    id         = Column(Integer, primary_key=True)
    docdate    = Column(DateTime)
    docnumber  = Column(String(10)) 
    userid     = Column(Integer, ForeignKey(User.id)) 
    author     = relationship(User)
    isheld     = Column(Boolean) 
    isdelete   = Column(Boolean) 
    
    def __init__(self, datecreate = datetime.now(), 
                 number = '0'):
        self.docdate    = datecreate
        self.docnumber  = number
        self.author     = None
        self.isheld     = False
#//////////////////////////////////////////////////////////////////////////////////////
class DocTableServiceTo1To2(Base):
    'Таблица документа назначения дня прохождения ТО-1 ТО-2'
    __tablename__ = 'doctableserviceto1to2'
    id              = Column(Integer, primary_key=True)
    docid           = Column(Integer, ForeignKey(DocumentServiceTo1To2.id))
    doc             = relationship(DocumentServiceTo1To2)
    busid           = Column(Integer, ForeignKey(Bus.id))     
    bus             = relationship(Bus)
    dt              = Column(Date)
    typeserviceid   = Column(Integer, ForeignKey(TypeService.id))
    typeservice     = relationship(TypeService) #тип обслуживания
    
    def __init__(self, doc, bus = None, dt = None, typeservice = None):
        self.doc         = doc
        self.bus         = bus
        self.dt          = dt
        self.typeservice = typeservice
#//////////////////////////////////////////////////////////////////////////////////////
class DocumentReadyServiceTo1To2(Base):
    'Документ назначения дня прохождения ТО-1 ТО-2'
    __tablename__ = 'docreadyserviceto1to2'
    id         = Column(Integer, primary_key=True)
    docdate    = Column(DateTime)
    docnumber  = Column(String(10)) 
    userid     = Column(Integer, ForeignKey(User.id)) 
    author     = relationship(User)
    isheld     = Column(Boolean) 
    isdelete   = Column(Boolean)
    
    def __init__(self, datecreate = datetime.now(), 
                 number = '0', userid = 1):
        self.docdate    = datecreate
        self.docnumber  = number
        self.userid     = userid
        self.isheld     = False
#//////////////////////////////////////////////////////////////////////////////////////
class DocTableReadyServiceTo1To2(Base):
    'Таблица документа назначения дня прохождения ТО-1 ТО-2'
    __tablename__ = 'doctablereadyserviceto1to2'
    id              = Column(Integer, primary_key=True)
    docid           = Column(Integer, ForeignKey(DocumentReadyServiceTo1To2.id))
    doc             = relationship(DocumentReadyServiceTo1To2)
    busid           = Column(Integer, ForeignKey(Bus.id))     
    bus             = relationship(Bus)
    dt              = Column(Date)
    km              = Column(Integer)
    typeserviceid   = Column(Integer, ForeignKey(TypeService.id))
    typeservice     = relationship(TypeService) #тип обслуживания
    
    def __init__(self, docid, busid = 0,  dt = None, km = 0, 
                 to1 = False, to2 = False):
        self.docid  = docid
        self.busid  = busid
        self.dt     = dt
        self.km     = km
        self.typeserviceid = 0

#//////////////////////////////////////////////////////////////////////////////////////
class VedomBusViezd(Base):
    'Ведомость контроля возвращения автобусов'
    __tablename__ = 'vedombusviezd'
    id      = Column(Integer, primary_key=True)
    busid   = Column(Integer, ForeignKey(Bus.id))     
    bus     = relationship(Bus)
    dt      = Column(DateTime)
    typedt  = Column(Integer)   #тип выезда 1 - выезд 1 смена план, 2 -выезд 1 смена факт 
    def __init__(self, busid, dt = datetime.now(), typedt = 1):
        self.busid  = busid
        self.dt     = dt
        self.typedt = typedt

class Dolg(Base):
    'Должности на предприятии'
    __tablename__ = 'dolg'
    id      = Column(Integer, primary_key=True)
    name    = Column(String(100))   
        
class Rabot(Base):
    'Работники предприятия'
    __tablename__ = 'rabot'
    id      = Column(Integer, primary_key=True)
    family  = Column(String(100))
    name    = Column(String(100))
    surname = Column(String(100))
    tabnmbr = Column(Integer)
    dolg    = relationship(Dolg)
    dolgid  = Column(Integer, ForeignKey(Dolg.id))
    dtbegin = Column(Date)
    dtend   = Column(Date)

class Karta(Base):
    'Карточки'
    __tablename__ = 'karta'
    id      = Column(Integer, primary_key=True)
    name    = Column(String(100))
    vihod   = Column(String(10))
    smena   = Column(Integer)
    namedbf = Column(Integer)

class Putev(Base):
    'Путевые листы'
    __tablename__ = 'putev'
    id      = Column(Integer, primary_key=True)
    bus     = relationship(Bus)
    busid   = Column(Integer, ForeignKey(Bus.id))
    nompl   = Column(Integer)
    dtviezd = Column(DateTime)
    dtvozvr = Column(DateTime)
    dtfactviezd = Column(DateTime)
    dtfactvozvr = Column(DateTime)
    smena   = Column(Integer)
    delete  = Column(Boolean)

class PutevRabot(Base):
    'Водители и другие работники (кондуктора, стажеры) с привязкой к путевому'
    __tablename__ = 'putevrabot'
    id      = Column(Integer, primary_key=True)
    putev   = relationship(Putev)
    putevid = Column(Integer, ForeignKey(Putev.id))
    rabot   = relationship(Rabot)
    rabotid = Column(Integer, ForeignKey(Rabot.id))

class PutevKarta(Base):
    'Привязка карточек к путевому листу'
    __tablename__ = 'putevkarta'
    id      = Column(Integer, primary_key=True)
    putev   = relationship(Putev)
    putevid = Column(Integer, ForeignKey(Putev.id))
    karta   = relationship(Karta)
    kartaid = Column(Integer, ForeignKey(Karta.id))

class PutevZakaz(Base):
    'Привязка к путевому листу заказных и др заданий'
    __tablename__ = 'putevzakaz'
    id      = Column(Integer, primary_key=True)
    putev   = relationship(Putev)
    putevid = Column(Integer, ForeignKey(Putev.id))
    adres  = Column(String(200))
    zakaz   = Column(String(200)) #TODO: необходима отдельная таблица
    
    