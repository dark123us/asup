# -*- coding: utf-8  -*-'''
from sqlalchemy import (Column, Integer, String, Time, Date, 
                        Interval, ForeignKey, DateTime, Float, Boolean,
                        engine_from_config, Table, MetaData 
                        )
from sqlalchemy.orm import (
    scoped_session,
    sessionmaker,
    relationship,
    mapper,
    )
from datetime import datetime, timedelta
from sqlalchemy.ext.declarative import declarative_base
from zope.sqlalchemy import ZopeTransactionExtension
import calendar
metadata = MetaData()
import logging, time
from sqlalchemy import func
from pprint import pprint
log = logging.getLogger(__name__)

KostylDBSession = scoped_session(sessionmaker(extension=ZopeTransactionExtension(),
                                        expire_on_commit=False))
KostylBase = declarative_base()


class KostylConnect():
    def __init__(self, url = 'mysql+mysqldb://root:234912234912@10.1.12.52/asdm?charset=utf8'):
        self._url = url
        settings = {}
        settings['sqlalchemy.url'] = self._url
        #settings['sqlalchemy.echo'] = echo
        #settings['sqlalchemy.connect_args'] = {'check_same_thread':False}
        engine = KostylEngine(settings)
        engine.configure()
    
    def disconnect(self):
        KostylDBSession.remove()
    
    def connect(self):
        settings = {}
        settings['sqlalchemy.url'] = self._url
        engine = KostylEngine(settings)
        engine.configure()

class KostylEngine():
    _engine = None
    def __init__(self, settings):
        '''settings = {}:
            'sqlalchemy.url':'sqlite:///:memory:'
            'sqlalchemy.echo':True
            '''
        self._engine = engine_from_config(settings, 'sqlalchemy.')
    def configure(self):
        KostylDBSession.remove()
        KostylDBSession.configure(bind=self._engine)
        KostylBase.metadata.bind = self._engine

# class KostylPribor(KostylBase):
#     ''
# 
# class KostylBus(KostylBase):
#     ''


# class KostylErrorShkiper(object):
#     def __init__(self):
#         self.id = 0
#         self.lat = 0.0
#         self.lon = 0.0
#         self.nmbr = 0
#         self.datetimepribor = datetime.now() 
#         self.datetimeprogramm = datetime.now()
#         self.sluj = 0
#         self.sluj2 = 0
#         self.koderror = 0  
#         self.mess = ''
# 
# class KostylDataShkiper(object):
#     ''

class Shkiper():
    'Класс работы с приборами навигации'
    def connect(self):
        self._k = KostylConnect()
        
    
    def disconnect(self):
        self._k.disconnect()

#     def _mapertableerror(self, year, month):
#         pribor_table = Table('error_{0:04d}{1:02d}'.format(year, month), metadata,
#                             Column('id', Integer, primary_key=True),
#                             Column('lat', Float),
#                             Column('lon', Float),
#                             Column('nmbr', Integer),
#                             Column('datetimepribor', DateTime),
#                             Column('datetimeprogramm', DateTime),
#                             Column('sluj', Integer),
#                             Column('sluj2', Integer),
#                             Column('koderror', Integer),   
#                             Column('mess', String(100)),
#                             )
#         return pribor_table
#     
#     def _mapertabledata(self, year, month):
#         pribor_table = Table('error_{0:04d}{1:02d}'.format(year, month), metadata,
#                             Column('id', Integer, primary_key=True),
#                             Column('lat', Float),
#                             Column('lon', Float),
#                             Column('nmbr', Integer),
#                             Column('datetimepribor', DateTime),
#                             Column('datetimeprogramm', DateTime),
#                             Column('npoint', Integer),
#                             )
#         return pribor_table

    
    def _splitdatetime(self, _from, _to, data = []):
        'разрезаем даты по месяцам'
        y1 = _from.year 
        m1 = _from.month
        y2 = _to.year
        m2 = _to.month
        if y1 != y2 or m1 != m2:
            self._splitdatetime(datetime(y1, m1, _from.day, _from.hour, _from.minute, _from.second), 
                                datetime(y1, m1, calendar.monthrange(y1, m1)[1], 23, 59, 59),
                                data)
            m1 = m1 + 1
            if m1 > 12:
                m1 = 1
                y1 += 1
            self._splitdatetime(datetime(y1, m1, 1, 0, 0, 0), 
                                datetime(y2, m2, _to.day, _to.hour, _to.minute, _to.second),
                                data)
        else:
            data.append([_from, _to])
        return data
        

    def _signalsql(self, dtbegin, dtend, prefix):
        'Создаем запрос на основании даты и времени'
        addvalue = ''
        if prefix == 'point_':
            addvalue = ' + sum(er.npoint)'
             
        sql = '''SELECT er.nmbr, count(er.id){5}, bus.id, bus.garnom
                 FROM {4}{0:04d}{1:02d} as er, pribor, bus
                 WHERE er.nmbr = pribor.nmbr AND pribor.idbus = bus.id AND
                       er.datetimeprogramm >= '{2}' AND 
                       er.datetimeprogramm <= '{3}'
                 GROUP BY er.nmbr
               '''.format(dtbegin.year, dtbegin.month, dtbegin, dtend, prefix, addvalue)
        return sql
    
    def _fillsignaldata(self, query, data):
        'Заполняем структуру данными из запроса'
        data = {}
        for row in query:
            if row[0] not in data:
                data[row[0]] = [row[1], row[2], row[3]] 
            else:  
                data[row[0]][0] += row[1]
        return data
    
    def countbreaksignal(self, _from = datetime.now()-timedelta(7), 
                               _to = datetime.now()):
        'Подсчитываем количество битых сигналов с группировкой по приборам'
        data = {}
        dtlist = self._splitdatetime(_from, _to, [])
        for dt in dtlist:
            try:
                sql = self._signalsql(dt[0], dt[1], 'error_')
                query = KostylDBSession.execute(sql)
                data = self._fillsignaldata(query, data)
            except Exception, e:
                log.error("{0}{1}".format(type(e), e))
        return data
    
    def counttruesignal(self, _from = datetime.now()-timedelta(7), 
                               _to = datetime.now()):
        'Подсчитываем количество нормальных сигналов с группировкой по приборам'
        data = {}
        dtlist = self._splitdatetime(_from, _to, [])
        for dt in dtlist:
            try:
                sql = self._signalsql(dt[0], dt[1], 'point_')
                query = KostylDBSession.execute(sql)
                data = self._fillsignaldata(query, data)
            except Exception, e:
                log.error("{0}{1}".format(type(e), e))
        return data
    
    def devicebus(self):
        data = {}
        try:
            sql = '''SELECT pribor.nmbr, bus.id, bus.garnom
                     FROM pribor, bus
                     WHERE pribor.idbus = bus.id
                     '''
            query = KostylDBSession.execute(sql)
            for row in query:
                data[row[0]] = [row[1], row[2]] 
        except Exception, e:
            log.error("{0}{1}".format(type(e), e))
        return data


        