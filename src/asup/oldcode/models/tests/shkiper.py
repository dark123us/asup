# -*- coding: utf-8  -*-'''
import logging
log = logging.getLogger(__name__)
import unittest, time
from models.shkiper import Shkiper
from datetime import datetime
from pprint import pprint

class TestShkiper(unittest.TestCase):


    def setUp(self):
        pass


    def tearDown(self):
        pass
 
    def test_connect(self):
        sh = Shkiper()
        sh.connect()
        data1 = sh.countbreaksignal(datetime(2013,9,10, 0, 0, 0), datetime(2013,9,10, 23, 59, 59))
        #sh.disconnect()
    
    def test_count(self):
        sh = Shkiper()
        sh.connect()
        t = time.time()
        data1 = sh.countbreaksignal(datetime(2013,9,10, 0, 0, 0), datetime(2013,9,10, 23, 59, 59))
        print len(data1)#
        pprint(data1)
        print time.time() - t
        
        t2 = time.time()
        data2 = sh.counttruesignal(datetime(2013,9,10, 12, 0, 0), datetime(2013,9,10, 23, 59, 59))
        print len(data2)#
        pprint(data2)
        print time.time() - t2

        #sh.disconnect()
 
    def test_spliter(self):
        sh = Shkiper()
        #sh.connect()
        #sh.breaksignal()
        d = sh._splitdatetime(datetime(2012,1,9, 12, 47, 11), datetime(2013,2,12, 11, 12, 10), [])
        self.assertEqual(d[0], [datetime(2012, 1, 9, 12, 47, 11), datetime(2012, 1, 31, 23, 59, 59)])
        self.assertEqual(d[13], [datetime(2013, 2, 1, 0, 0), datetime(2013, 2, 12, 11, 12, 10)])
    
            

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()