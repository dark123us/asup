# -*- coding: utf-8  -*-'''
from datetime import datetime, timedelta, date
from engine.dictionary.model import Model
import transaction
from orm import (
    DBSession,
    Bus, 
    Putev, 
    PutevKarta, 
    PutevRabot, 
    PutevZakaz,
    )

from models.shkiper import Shkiper 
from sqlalchemy import func, text, and_
import logging 
from datetime import datetime
log = logging.getLogger(__name__)

class ModelMehanik(Model):
    '''Модель механика
    _data - данные в виде словаря, верхний уровень - гаражный:
        ["bus"] - данные по автобусу ORM модель BUS
        ['plan'] - 
        ['putev'] - путевые листы orm putev
            [1] - 1-я смена
            [2] - 2-я смена
        ['shkiper'] - данные по шкиперу
            ['nmbr'] - номер прибора
            ['true'] - передано верных данных
            ['false'] - передано битых данных
            
    '''
    _data = {}
    _garnmdoid = {} #ключ гаражный значение ид записи
    def _getbus(self):
        'Получаем данные об автобусах'
        query = DBSession.query(Bus).all()
        self._data = {}
        self._garnmdoid = {}
        for row in query:
            self._data[row.id] = {}
            self._data[row.id]['bus'] = row
            self._garnmdoid[row.garnm] = row.id 
            self._data[row.id]['plan'] = [None, None, None, None]
            self._data[row.id]['fact'] = [None, None, None, None]
            self._data[row.id]['putev'] = {}
    
    def _getdate(self, rowstr):
        if rowstr:
            return datetime.strptime(rowstr, '%Y-%m-%d %H:%M:%S.%f')
        else:
            return None

    def _getputev(self, curdate, smena):
        '''Запрос с подзапросом возвращаем путевые, которые пересекают данный день 
        Смена в запросе для того чтобы получить раздельно данные для 1-й смены и 2-й смены'''
        curdate = date(curdate.year, curdate.month, curdate.day)
        curdate1 = curdate + timedelta(1,0)  
        subquery = DBSession.query(Putev.busid,
                                   func.max(Putev.dtviezd).label('maxdt'))\
                            .filter(and_(Putev.dtviezd <= curdate1,
                                         Putev.dtvozvr >= curdate,
                                         Putev.smena == smena))\
                            .group_by(Putev.busid)\
                            .subquery()
        query = DBSession.query(Putev).filter(and_(Putev.busid == subquery.c.busid,
                                                   Putev.dtviezd == subquery.c.maxdt)).all()
        return query
    
    def _gettimeplanfact(self, curdate = None):
        'Получаем список путевых, пересекающих текущий период'
        if not curdate:
            curdate = datetime.now()
        for smena in (1,2):
            query = self._getputev(curdate, smena) 
            for row in query:
                try:
                    col = smena - 1  
                    self._data[row.busid]['plan'][col * 2] = row.dtviezd 
                    self._data[row.busid]['plan'][col * 2 + 1] = row.dtvozvr
                    
                    self._data[row.busid]['fact'][col * 2] = row.dtfactviezd 
                    self._data[row.busid]['fact'][col * 2 + 1] = row.dtfactvozvr
                    
                    self._data[row.busid]['putev'][smena] = {}
                    self._data[row.busid]['putev'][smena]['putev'] = row
                except KeyError:
                    logging.info('not found busid = {0}'.format(row.busid))
    
    def _getservice(self, curdate = None):
        'Расширяем данные обслуживанием (ТО, страховки и др.)'
    
    def _getshkiper(self, curdate = None):
        'Расширяем данные работой шкиперов'
        if '_shkiper' not in self.__dict__: #если обращение в первый раз - читаем данные за неделю
            self._shkiper = {}
            self._shkiperobject = Shkiper()
            self._shkiperobject.connect()
            dt = datetime.now()
            dayago = 7 # на сколько дней смотреть назад - чем больше, тем больше загрузка
            data1 = self._shkiperobject.countbreaksignal(dt - timedelta(dayago), dt)
            data2 = self._shkiperobject.counttruesignal(dt - timedelta(dayago), dt)
            data3 = self._shkiperobject.devicebus()
            self._shkiperlastdt = dt
            for key in data3.keys():
                garnom = data3[key][1]
                if garnom in self._garnmdoid:
                    idbus = self._garnmdoid[garnom]
                    nmbr = key
                    self._shkiper[garnom] = {}
                    self._shkiper[garnom]['nmbr'] = nmbr 
                    self._shkiper[garnom]['idbus'] = idbus
                    self._shkiper[garnom]['false'] = 0
                    self._shkiper[garnom]['true'] = 0
                
                
        else:                               #иначе только за истекший период
            dt = datetime.now()
            data1 = self._shkiperobject.countbreaksignal(self._shkiperlastdt + timedelta(0,1), dt)
            data2 = self._shkiperobject.counttruesignal(self._shkiperlastdt + timedelta(0,1), dt)
            self._shkiperlastdt = dt
        
        for key in data1.keys(): #заполняем плохие пакеты
            garnom = data1[key][2]
            cbad = data1[key][0]
            nmbr = key
            self._shkiper[garnom]['false'] += cbad
        
        for key in data2.keys(): #заполняем хорошие пакеты
            garnom = data2[key][2]
            cgood = data2[key][0]
            nmbr = key
            self._shkiper[garnom]['true'] += cgood
        
        for key in self._shkiper.keys():
            idbus = self._shkiper[key]['idbus']
            if idbus in self._data:
                self._data[idbus]['shkiper'] = {}
                self._data[idbus]['shkiper']['nmbr'] = self._shkiper[key]['nmbr'] 
                self._data[idbus]['shkiper']['true'] = self._shkiper[key]['true']
                self._data[idbus]['shkiper']['false'] = self._shkiper[key]['false']
    
    def _getstate(self, curdate = None):
        'Расширяем данные состоянием - ремонт, на ТО-1, резерв'
    
    def _getdata(self, dt = None):
        'Обновление, чтение всех данных'
        try:
            DBSession.commit()
        except Exception, e:
            logging.error("{0} {1}".format(type(e),e))
            DBSession.rollback() 
        self._getbus()
        self._gettimeplanfact(dt)
        self._getservice(dt)
        self._getshkiper(dt)
        self._getstate(dt)
        self.notifyobservers()
    
    def _getkarta(self, putevid):
        'Получаем карточки для путевого'
        query = DBSession.query(PutevKarta).filter(PutevKarta.putevid == putevid)
        return query
    
    def _getrabot(self, putevid):
        'Получаем список работников, работающих по путему'
        query = DBSession.query(PutevRabot).filter(PutevRabot.putevid == putevid)
        return query
    
    def settimefact(self, busid, dtfact, typefact):
        '''Устанавливаем фактическое время typefact - тип фактического
        времени'''

    def updatedata(self, dt = None):
        self._getdata(dt)
    
    def getdata(self):
        'Возвращаем данные по внешнему запросу'
        return self._data
    
    def getdatabus(self, garnm):
        'Возвращаем данные по выбранному гаражному'
        ret = None
        try:
            ret = self._data[self._garnmdoid[garnm]]
            if ret['putev']:
                for i in (1,2):
                    if i in ret['putev'].keys():
                        putevid = ret['putev'][i]["putev"].id
                        karta = self._getkarta(putevid)
                        if karta:
                            ret['putev'][i]['karta'] = karta
                        rabot = self._getrabot(putevid)
                        if rabot:
                            ret['putev'][i]['rabot'] = rabot
        except KeyError:
            logging.debug('not found in model garnm = {0}'. format(garnm))
        self._tmpdata = ret
        return ret
    
    def setfactviezd(self):
        for row in self._tmpdata['putev'].keys():
            putev = self._tmpdata['putev'][row]["putev"]
            if putev.dtfactviezd is None:
                break
        putev.dtfactviezd = datetime.now()
        with transaction.manager:
            DBSession.add(putev)
    
    def setfactvozvr(self):
        ''
        for row in self._tmpdata['putev'].keys():
            putev = self._tmpdata['putev'][row]["putev"]
            if putev.dtfactvozvr is None:
                break
        putev.dtfactvozvr = datetime.now()
        with transaction.manager:
            DBSession.add(putev)
    
#===============================================================================
# Тестовый юнитs        
#===============================================================================


#    
#    def header(self):
#        'Отдаем заголовок'
#        head = []
#        self.colcol = (self.flags[0] + self.flags[1]) * 2 
#        for i in range(self.columnview):
#            head += [u'Гар']
#            if self.flags[0]:
#                head += [u'1см\nвыезд\nплан\n(факт)']
#                head += [u'1см\nвозвр\nплан\n(факт)']
#            if self.flags[1]:
#                head += [u'2см\nвыезд\nплан\n(факт)']
#                head += [u'2см\nвозвр\nплан\n(факт)']
#        return head
#    
#    def gethour(self, hour):
#        'По полученному времени возвращаем строку'
#        h = ''
#        if hour:
#            h = datetime.strftime(hour, "%H:%M")
#        return h 
#    
#    def cleardataview(self, rowmax):
#        'Подготавливаем данные для отображения'
#        self.dataview = []
#        #создаем пустую сетку
#        for rows in range(rowmax):
#            row = []
#            for cols in range(self.columnview):
#                col = ['']
#                for data in range(self.colcol+1):#гаражный и 8 таймеров
#                    col += ['']
#                row += col
#            self.dataview += [row]
#    
#    def getstrviezd(self, time1, time2):
#        'По времени плановому и фактическому формируем строку'
#        if self.gethour(time2):
#            st = "%s(%s)"%(self.gethour(time1),self.gethour(time2))
#        else:  
#            st = "%s"%(self.gethour(time1))
#        return st
#    
#    def datatodataview(self):
#        'Преобразоваываем данные в представление'
#        self.colcol = (self.flags[0] + self.flags[1]) * 2
#        rowmax = len(self.data)/self.columnview + 1
#        self.cleardataview(rowmax)
#        crow = 0
#        ccol = 0
#        col = 0
#        for rec in self.data:
#            if crow > rowmax - 1:
#                ccol += 1
#                crow = 0
#                col = ccol * (self.colcol + 1)
#            #в зависимости от флагов отображаем данные
#            inccol = 0    
#            self.dataview[crow][col] = rec[0] #гаражный
#            if self.flags[0]:
#                inccol += 1
#                stvd1 = self.getstrviezd(rec[2], rec[3])
#                self.dataview[crow][col + inccol] = stvd1
#                inccol += 1
#                stvr1 = self.getstrviezd(rec[4], rec[5])
#                self.dataview[crow][col + inccol] = stvr1
#            if self.flags[1]:                            
#                inccol += 1
#                stvd2 = self.getstrviezd(rec[6], rec[7])
#                self.dataview[crow][col + inccol] = stvd2
#                inccol += 1
#                stvr2 = self.getstrviezd(rec[8], rec[9])
#                self.dataview[crow][col + inccol] = stvr2
#            crow += 1
#    
#    def readrecords(self):
#        '''Чтение данных - создание таблицы данных.
#        
#        Считываем автобусы, по свойству columnview определяем на сколько колонок разбить
#        и заполняем таблицу данными гаражный, время выезда, время возвращения
#        typedt принимаем, что типы идут в следующем порядке:
#         
#            * 1 - выезд 1 см план
#            * 2 - выезд 1 см факт
#            * 3 - возвращение 1 см план
#            * 4 - возвращение 1 см факт
#            * 5 - выезд 2 см план
#            * 6 - выезд 2 см факт
#            * 7 - возвращение 2 см план
#            * 8 - возвращение 2 см факт
#        
#        в data заносим в следуюещем порядке (по колонкам):
#            * гаражный для сортировки
#            * запись с данными по автобусу (запись таблицы bus)
#            * следущие 8 колонки согласно typedt 
#        '''
#        self.data = []
#        bus = {}
#        query = self.session.query(Bus).all()
#        for row in query:
#            tmp = [row.garnm, row]
#            for i in range(8):
#                tmp += [None]
#            bus[row.id] = tmp
#        query =  self.session.query(VedomBusViezd)
#        for row in query:
#            try:
#                bus[row.busid][row.typedt+1] = row.dt #typedt тип времени
#            except KeyError:
#                print "in VedomBusViezd Not found bus with key",row.busid
#        for key in bus.keys():
#            self.data += [bus[key]]
#        self.data = sorted(self.data) #сортируем по гаражному
#        self.datatodataview()
#        self.notifyobserver()
#    
#    def getbus(self, garnm):
#        'По гаражному номеру получаем id'
#        return self.session.query(Bus).filter(Bus.garnm == garnm).first()
#    
#    def setview(self, flag, state):
#        '''Установка флагов, как хотим видеть данные'''
#        self.flags[flag] = state
#        if state != 2:#если это предпросмотр нет смысла перерисовывать
#            self.datatodataview()
#            self.notifyobserver()
