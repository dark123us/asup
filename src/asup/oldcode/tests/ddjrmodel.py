# -*- coding: utf-8  -*-

#===============================================================================
# Тестовый юнитs        
#===============================================================================
import unittest, threading, socket
from engine.dictionary.model import Model


class TestDDJRModel(unittest.TestCase):
    def test_modelsingl(self):
        addr1 = Model()
        addr2 = Model()
        self.assertEqual(addr1, addr2, 'error in singlenton model')
    
    def test_modelobserver(self):
        addr1 = Model()
        addr1.appendobserver(self)
        addr2 = Model()
        addr2.appendobserver(self)
        self.assertEqual(len(addr2._observers), 2, 'error in appending observer')
        addr2.removeobserver(self)
        self.assertEqual(len(addr2._observers), 1, 'error in remove observer')
        addr2.removeobserver(self)
        self.assertEqual(len(addr2._observers), 0, 'error in remove observer')
        