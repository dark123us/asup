import unittest, threading, socket
import sqlalchemy
from orm.orm import User
from engine.log import Log
import os

class TestConnectModelMehanik(unittest.TestCase):
    def test_errorconnect(self):
        engine = SinglEngine()
        engine.reconnect()
        model = ModelMehanik()
        auth = ['mysql+mysqldb','10.1.12.52','asuptest','asuptest','123451']
        model.setconnect(auth)
        with self.assertRaises(sqlalchemy.exc.OperationalError):
            query = model._session.query(User).all() 
    
    def test_connect(self):
        engine = SinglEngine()
        engine.reconnect()
        model = ModelMehanik()
        auth = ['mysql+mysqldb','10.1.12.52','asuptest','asuptest','12345']
        model.setconnect(auth)
        query = model._session.query(User).all()
    

class TestModelMehanik(unittest.TestCase):
    _testinsqlite = False
    @classmethod
    def setUpClass(self):
        try:
            Log()
        except:
            Log('/../../')
        engine = SinglEngine()
        engine.reconnect()
        engine = SinglEngine()
        engine.setconnectionsqlitememory(False)
        self._testinsqlite = True
        engine.createdatabase()
        self._session = engine.getsession()
        bus1 = Bus()
        bus1.id = 1
        bus1.garnm = 123
        self._session.add(bus1)
        bus2 = Bus()
        bus2.id = 2
        bus2.garnm = 124
        self._session.add(bus2)
        
        putev1 = Putev()
        putev1.bus = bus1
        putev1.dtviezd = datetime(2013,5,17,12,0)
        putev1.dtvozvr = datetime(2013,5,17,17,0)
        putev1.nompl = 1
        putev1.smena = 2
        self._session.add(putev1)
        
        putev2 = Putev()
        putev2.bus = bus2
        putev2.dtviezd = datetime(2013,5,18,6,0)
        putev2.dtvozvr = datetime(2013,5,18,17,0)
        putev2.nompl = 2
        putev2.smena = 1
        self._session.add(putev2)
        
        putev3 = Putev()
        putev3.bus = bus1
        putev3.dtviezd = datetime(2013,5,18,10,0)
        putev3.dtvozvr = datetime(2013,5,18,17,20)
        putev3.nompl = 3
        putev3.smena = 1
        self._session.add(putev3)
        
        self._session.commit()
        model = ModelMehanik()
        auth = ['','','','','']
        model.setconnect(auth)
    
    def test_getbus(self):
        model = ModelMehanik()
        model._getbus()
        self.assertEqual(len(model._data), 2, 'error in getbus')
        self.assertEqual(model._data[1]['bus'].garnm, 123, 'error in getbus')
    
    def test_gettimeplan(self):
        model = ModelMehanik()
        model._getbus()
        model._gettimeplan(datetime(2013,5,18))
        val = model._data[1]["plan"][0]
        if self._testinsqlite:
            valsql = val# datetime.strptime(val, "%Y-%m-%d %H:%M:%S.%f")
            self.assertEqual(valsql, datetime(2013,5,18,10,0), 'error in gettimeplan {0} type =  {1}'. format(val, type(val)))
        else:
            self.assertEqual(val, datetime(2013,5,18,10,0), 'error in gettimeplan may be in sqlite {0} type =  {1}'. format(val, type(val)))

    def test_getbusdata(self):
        model = ModelMehanik()
        model.getdata()
        bus = model.getdatabus(0)
        self.assertIsNone(bus, u'возвращено не пустое значение')
        bus = model.getdatabus(124)
        self.assertEquals(bus["bus"].id, 2, u'неверно вернулись данные')
    
    @classmethod
    def tearDownClass(self):
        ''' '''

