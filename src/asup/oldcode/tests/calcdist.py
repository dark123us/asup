#!/usr/bin/python
# -*- coding: utf-8  -*-'''
import math
import time

def calcdist(llat1, llong1, llat2, llong2, converttodeg = True): 
    #pi - число pi, rad - радиус сферы (Земли)
    rad = 6372795
     
    pi180 = math.pi/180.
     
#     #в радианах
#     lat1 = llat1*math.pi/180.
#     lat2 = llat2*math.pi/180.
#     long1 = llong1*math.pi/180.
#     long2 = llong2*math.pi/180.
    
    #в радианах
    lat1 = llat1*pi180
    lat2 = llat2*pi180
    long1 = llong1*pi180
    long2 = llong2*pi180
     
    #косинусы и синусы широт и разницы долгот
    cl1 = math.cos(lat1)
    cl2 = math.cos(lat2)
    sl1 = math.sin(lat1)
    sl2 = math.sin(lat2)
    delta = long2 - long1
    cdelta = math.cos(delta)
    sdelta = math.sin(delta)
     
    #вычисления длины большого круга
    y = math.sqrt(math.pow(cl2*sdelta,2)+math.pow(cl1*sl2-sl1*cl2*cdelta,2))
    x = sl1*sl2+cl1*cl2*cdelta
    ad = math.atan2(y,x)
    dist = ad*rad
     
    #вычисление начального азимута
    x = (cl1*sl2) - (sl1*cl2*cdelta)
    y = sdelta*cl2
    z = math.degrees(math.atan(-y/x))
     
    if (x < 0):
        z = z+180.
     
    z2 = (z+180.) % 360. - 180.
    z2 = - math.radians(z2)
    anglerad2 = z2 - ((2*math.pi)*math.floor((z2/(2*math.pi))) )
    if converttodeg:
        angledeg = (anglerad2*180.)/math.pi
        return dist, angledeg
    else:
        return dist, anglerad2

def main():
    #координаты двух точек
    llat1 = 55 + 45/60.
    llong1 = 37 + 37/60.
     
    llat2 = 59 + 53/60.
    llong2 = 30 + 15/60.
    t = time.time()
    for row in range(200000):
        r = calcdist(llat1, llong1, llat2, llong2, True)
    print time.time() - t
    print r 
    

if __name__ == "__main__":
    main()
    
