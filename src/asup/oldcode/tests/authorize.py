#!/usr/bin/python
# -*- coding: utf-8  -*-'''
#===============================================================================
# Тестовый юнитs        
#===============================================================================
import unittest, threading, socket
import md5, os, cPickle
import socket, asyncore
from server.authorize import Authorize
import logging
from engine.log import Log                        
from models.orm import (
    DBSession,
    Engine,
    User, 
    GroupUser,
    )
from sqlalchemy.orm import (
    scoped_session,
    sessionmaker,
    )
 
class ThreadLoop(threading.Thread):
    def run(self):
        asyncore.loop(1)

class TestAuthorize(unittest.TestCase):
    _server = None
    _ip = '127.0.0.1'
    _port = 31001
    _settings = {} 
    _gradmin = None
    _user = None 
    @classmethod
    def setUpClass(self):
        Log()
        self._settings = {}
        #self._settings['sqlalchemy.url'] = 'sqlite:///test.sqlite'
        #self._settings['sqlalchemy.echo'] = False
        #self._settings['sqlalchemy.connect_args'] = {'check_same_thread':False}
        
        self._settings['sqlalchemy.url'] = 'mysql+mysqldb://asuptest:12345@10.1.12.52/asuptest?charset=utf8'
        
        engine = Engine(self._settings)
        engine.configure()
        engine.createbase([User.__table__, GroupUser.__table__])#создаем только 2 таблицы
        
        self._gradmin = GroupUser()
        self._gradmin.name     = u'ТестАдминистратор'
        self._gradmin.dbengine = 'mysql+mysqldb'
        self._gradmin.host     = '10.1.12.52'
        self._gradmin.dbname   = 'asuptest'
        self._gradmin.user     = 'asuptest'
        self._gradmin.password = '12345'
        self._gradmin.component = 2
        DBSession.add(self._gradmin)
        self._user = User()
        self._user.name = u'Пригодич Тест'
        m = md5.new()
        m.update('12345')
        self._user.password = m.hexdigest()
        self._user.group = self._gradmin 
        DBSession.add(self._user)
        DBSession.commit()

        self._server = Authorize()
        self._server._readconfig()
        
        config = {}
        config['AUTHORIZE'] = {}
        config['AUTHORIZE']['ip'] = self._ip
        config['AUTHORIZE']['port'] = self._port
        
        self._server._config = config
        self._server.startserver()
        ThreadLoop().start()
    
    @classmethod
    def tearDownClass(self):
        DBSession.delete(self._gradmin)
        DBSession.delete(self._user)
        DBSession.commit()
        self._server.close()
        asyncore.close_all()
        #os.remove('test.sqlite')
        
    def test_config_file(self):
        server = Authorize()
        configPath = os.getcwd() + server._path + '/config.ini'
        expr = os.path.exists(configPath)
        self.assertTrue(expr, 'file config not found {0}'.format(configPath))
    
    def test_crtable(self):
        user = DBSession.query(User).first()
        self.assertEqual(user.id, 1)
    
    def test_server(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((self._ip, self._port))
        try:            
            pack = [u'Пригодич Тест', '123']
            s.send(cPickle.dumps(pack))
            read = cPickle.loads(s.recv(1024))
            self.assertEqual(type(read), str)
            self.assertEqual(read, 'Access denied')
            pack = [u'Пригодич Тест', '12345']
            s.send(cPickle.dumps(pack))
            read = cPickle.loads(s.recv(1024))
            self.assertEqual(len(read), 6)
        except Exception,e:
            logging.error("{0}{1}".format(type(e),e)) 
            raise     
        finally:    
            s.close()
    
    def test_updatesession(self):
        settings = self._settings
        engine = Engine(settings)
        session = scoped_session(sessionmaker(bind=engine._engine, autoflush=True))
        user = session.query(User).filter(User.name == u'Пригодич Тест').first()
        m = md5.new()
        m.update('123')
        user.password = m.hexdigest() 
        session.commit()
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((self._ip, self._port))
        try:            
            pack = [u'Пригодич Тест', '1234']
            s.send(cPickle.dumps(pack))
            read = cPickle.loads(s.recv(1024))
            self.assertEqual(type(read), str)
            self.assertEqual(read, 'Access denied')
            pack = [u'Пригодич Тест', '123']
            s.send(cPickle.dumps(pack))
            read = cPickle.loads(s.recv(1024))
            self.assertEqual(len(read), 6)
        except Exception,e:
            logging.error("{0}{1}".format(type(e),e))
            raise      
        finally:    
            s.close()
            
    
if __name__ == "__main__":
    #unittest.main()
    suite = unittest.TestLoader().loadTestsFromTestCase(TestAuthorize)
    unittest.TextTestRunner(verbosity=2).run(suite)
    