# -*- coding: utf-8  -*-'''
import unittest
from models.orm import (
    Engine, 
    DBSession,
    Bus,
    Marka,
    TypeService,
    )
import time
from sqlalchemy.orm import (
    sessionmaker,
    )
import logging
log = logging.getLogger(__name__)

class TestOrmModel(unittest.TestCase):
    '''Тестируем работу сессии, т.к. почему-то сессия не обновляет данные
    Пример - в программе получаем  данные, во внешней данные в базе изменяются
    а вот при перечтении данных данные получаем старые'''
    
    @classmethod
    def setUpClass(self):
        settings = {}
        settings['sqlalchemy.url'] = 'sqlite:///:memory:'
        settings['sqlalchemy.echo'] = False
        settings['sqlalchemy.connect_args'] = {'check_same_thread':False}
        engine = Engine(settings)
        engine.configure()
        engine.createbase([Bus.__table__, Marka.__table__])#создаем только 2 таблицы
        bus = Bus()
        bus.garnm = 123
        DBSession.add(bus)
        DBSession.commit()

    def test_dbsession(self):
        bus = DBSession.query(Bus).first()
        self.assertEqual(bus.garnm, 123)
    
    def test_createall(self):
        try:
            ts = DBSession.query(TypeService)
            ts.all()
            self.assertIsNone(ts)
        except:
            self.assertIsNotNone(ts)


    def test_updatedata(self):
        secswait = 0
        if not secswait:
            log.warn('for work with test change secswait on more value what 0')
        else:
            settings = {}
            settings['sqlalchemy.url'] = 'sqlite:///test.sqlite'
            settings['sqlalchemy.echo'] = False
            settings['sqlalchemy.connect_args'] = {'check_same_thread':False}
            engine = Engine(settings)
            Session = sessionmaker(bind = engine._engine)
            session = Session() 
            engine.createbase()
            bus = Bus()
            bus.garnm = 123
            session.add(bus)
            session.commit()
            logging.warn('begin long test ({0} secs) for testing update value'.format(secswait))
            time.sleep(secswait)
            logging.warn('end long test for update value')
            bus = session.query(Bus).first()
            self.assertEqual(bus.imei, 123)
        
        

if __name__ == "__main__":
    #import sys;sys.argv = ['', 'Test.testName']
    unittest.main()