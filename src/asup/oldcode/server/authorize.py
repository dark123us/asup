#!/usr/bin/python
# -*- coding: utf-8  -*-'''
''' Сервер для авторизации пользователей, после верной авторизации 
возвращаются данные по подключению к базе данных, авторизация происходит
по tcp/ip потоку, передаются данные в виде логин и пароль
для запуска сервера:: 
    
    server = Authorize()
    server.startserver()
    asyncore.loop(1)
    
Подключение к серверу для авторизации::

    HOST = '127.0.0.1'    # The remote host
    port = 21004              # The same port as used by the server
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((config.ip, config.port))
    pack = [u'Пригодич', '123']
    s.send(cPickle.dumps(pack))
    read = cPickle.loads(s.recv(1024))
    if type(read) == str:
        if read in 'Access denied':
            print 'Access denied'
    else:
        dbengine = read[0]
        host     = read[1]
        dbname   = read[2]
        dbuser   = read[3] 
        password = read[4]
        component= read[5]

'''
from models.orm import (
    DBSession,
    User, 
    GroupUser,
    )

import os, codecs

from configobj import ConfigObj
import socket, asyncore
import md5, sys
import time
import cPickle
import logging
from PyQt4 import QtGui, QtCore
import locale
enc = locale.getpreferredencoding()

class _Params():
    'Структура получаемых параметров от сервера авторизации'
    ip          = ''
    port        = 31000
    dbengine    = 'mysql+mysqldb'
    host        = '10.1.12.52'
    dbname      = 'asuptest'
    user        = 'asuptest'
    password    = '12345'
    echo        = False
    component   = 'mehanik'

class AuthorizeThread(asyncore.dispatcher):
    'Поток работы с клиентом'
    _observers = []
    _timestop = 120
    _buffer_read  = ''
    _buffer_write = ''

    def __init__(self, conn):
        asyncore.dispatcher.__init__(self, conn)
        self._timer = time.time()
    def addobserver(self, inobserver):
        "Паттерн наблюдатель - добавляем наблюдателя"
        self._observers.append(inobserver)
    def removeobserver(self, inobserver):
        "Паттерн наблюдатель - удаляем наблюдателя"
        self._observers.remove(inobserver)
    def _notifyobservertoclose(self):
        for observer in self._observers:
            observer.threadclose(self)
    def handle_read(self):
        '''Метод для чтения пакетов '''
        read = self.recv(1024)
        if len(read) > 0:
            self._buffer_read += read
            try:
                data = cPickle.loads(self._buffer_read)
                dbconnect = self._getuser(data[0], data[1])
                if dbconnect:
                    self._buffer_write = cPickle.dumps(dbconnect)
                else:
                    self._buffer_write = cPickle.dumps('Access denied')
            except Exception, e:
                logging.debug("{0}{1}".format(type(e), e))
                self._buffer_write = cPickle.dumps('Access denied')
    def handle_close(self):
        '''При закрытии потока прибором сообщаем слушающему серверу о данном событии'''
        self._notifyobservertoclose()
        self.close()
    def writable(self):
        'Обработка таймаута'
        if time.time() - self._timer > self._timestop :
            return True#Таймер перевалил за 2мин - посылаем истину
        else:
            return (len(self._buffer_write) > 0)
    def handle_write(self):
        'Возвращаем значение прибору'
        if (len(self._buffer_write) > 0):
            logging.debug("send message {0}".format(self._buffer_write))
            sent = self.send(self._buffer_write)
            self._buffer_write = self._buffer_write[sent:]
            self._buffer_read = ''
        else:
            self.handle_close()
    def _getuser(self, username, password):
        'По логину паролю получаем данные по подключению для пользователя'
        try:
            m = md5.new()
            m.update(password)
            logging.debug(u"begin find user = [{0}] password = [{1}]".format(username, m.hexdigest()))
            try:
                DBSession.commit()
            except Exception, e:
                logging.error("{0} {1}".format(type(e),e))
                DBSession.rollback()
            result =  DBSession.query(User, GroupUser).\
                join(GroupUser).filter(User.name == username).first()

            logging.debug(u"result query orm {0}".format(result))
            ret = []
            if result:
                user, gruser = result 
                logging.debug('user finded, checking password')
                m = md5.new()
                m.update(password)
                if user.password == m.hexdigest():
                    logging.debug('password checked true')
                    ret.append(gruser.dbengine)
                    ret.append(gruser.host)
                    ret.append(gruser.dbname)
                    ret.append(gruser.user)
                    ret.append(gruser.password)
                    ret.append(gruser.component)
        except Exception as e:
            logging.critical('{0} {1} \n inputvalue username = {2} password = {3}'.format(type(e), e, username, password))
            raise
        return ret

class Authorize(asyncore.dispatcher):
    '''Класс авторизации пользователей по подключению tcp/ip
        '''
    _path = ''
    _clients = []
    _config = []
    
    def _readconfig(self):
        'Читаем данные по авторизации'
        configPath = os.getcwd() + self._path + '/config.ini'
        self._config = ConfigObj(configPath)
    
    def startserver(self):
        'Запуск сервера'
        if not self._config:
            self._readconfig()
        try:
            ip   = self._config['AUTHORIZE']['ip']
            port = int(self._config['AUTHORIZE']['port'])
            self.create_socket(socket.AF_INET,socket.SOCK_STREAM)
            self.set_reuse_addr()
            self.bind((ip, port))
            self.listen(100)
            logging.debug('Start server {0}:{1}'.format(ip, port))
        except Exception, e:
            logging.error('{0}:{1}'.format(type(e), e))
            raise
    
    def handle_accept(self):
        '''Получаем соединение, включаем поток'''
        conn, addr = self.accept()#создаем поток для прослушивания
        tmp = AuthorizeThread(conn)
        self._clients.append(tmp)
    
    def close(self):
        for client in self._clients:
            client.handle_close()
        asyncore.dispatcher.close(self)
        logging.debug('Stop server')

class ClientAuthorizeWindow(QtGui.QDialog):
    _host = '127.0.0.1'
    _port = 31000
    _data = None
    def __init__(self, *args, **kwargs):
        QtGui.QDialog.__init__(self, *args, **kwargs)
        self.initwidget()
        self.setcentral()
        self.setWindowTitle(u'Авторизация')
    
    def setaddr(self, host, port):
        self._host = host
        self._port = port
    
    def setcentral(self):
        screen = QtGui.QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move((screen.width()-size.width())/2,
            (screen.height()-size.height())/2)
    
    def initwidget(self):
        self._lblmessage = QtGui.QLabel()
        lbllogin = QtGui.QLabel(u'Логин')
        self._edlogin = QtGui.QLineEdit('')
        lblpasswd = QtGui.QLabel(u'Пароль')
        self._edpasswd = QtGui.QLineEdit('')
        self._edpasswd.setEchoMode(QtGui.QLineEdit.Password)
        dbutton = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok | QtGui.QDialogButtonBox.Cancel)
        self.connect(dbutton, QtCore.SIGNAL("rejected()"),self.close) 
        self.connect(dbutton, QtCore.SIGNAL("accepted()"),self.accept)
        
        grl = QtGui.QGridLayout()
        grl.addWidget(self._lblmessage, 0, 0,1,2)
        grl.addWidget(lbllogin, 1, 0)
        grl.addWidget(self._edlogin, 1, 1)
        grl.addWidget(lblpasswd, 2, 0)
        grl.addWidget(self._edpasswd, 2, 1)
        grl.addWidget(dbutton, 3, 0, 1, 2)
        self.setLayout(grl)
        self._edpasswd.setFocus()
        #w = QtGui.QWidget()
        #w.setLayout(grl)
        #self.setCentralWidget(w)
    
    def _cqstos(self, widget):
        return str(widget.toUtf8()).decode("utf-8")

    def _connectserver(self):
        ''
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        read = None
        err = None
        try:
            s.connect((self._host, self._port))
            login = self._cqstos(self._edlogin.text())
            passwd = self._cqstos(self._edpasswd.text())
            pack = [login, passwd]
            s.send(cPickle.dumps(pack))
            logging.debug("begin listen to server")
            read = s.recv(1024)
            buf = read
            read = cPickle.loads(buf)
            s.close()
        except socket.error, e:
            logging.error(("socket.error {0}".format(e)).decode(enc))
            err = u'Сервер не доступен'
        except Exception, e:
            logging.error("{0} {1}".format(type(e), e))
        if read == 'Access denied':
            read = None
            err = u'Неверный логин или пароль'
        return read, err
    
    def accept(self):
        'Проверяем логин и пароль'
        self._data, err = self._connectserver()
        if self._data:
            self.close()
        else:
            if err:
                self._lblmessage.setText(err)
            else:
                self._lblmessage.setText(u'Неверный логин или пароль')
        
    def getdata(self):
        return self._data
    
    def setlastlogin(self, lastlogin):
        self._edlogin.setText(lastlogin)
    
    def getlastlogin(self):
        return self._edlogin.text()

class ClientAuthorize():
    '''Графический клиент авторизации, показываем графическое окно, возвращаем
    данные по авторизации либо None, если отказались авторизовываться 
    '''
    _path = ''
    def start(self):
        app = QtGui.QApplication(sys.argv)
        mw = ClientAuthorizeWindow()#MainWindow()
        configPath = os.getcwd() + self._path + '/config.ini'
        config = ConfigObj(configPath, encoding = "UTF8")
        mw.setaddr(config['AUTHORIZE']['ip'], int(config['AUTHORIZE']['port']))
        try:
            mw.setlastlogin(config['AUTHORIZE']['lastlogin'])
        except Exception, e:
            logging.debug("{0} {1}".format(type(e),e))
        mw.show()
        app.exec_()
        value = str(mw.getlastlogin().toUtf8()).decode("utf-8")
        
        logging.debug(value)
        config = ConfigObj(configPath, encoding = "UTF8")
        config['AUTHORIZE']['lastlogin'] = value
        config.write()
        return mw.getdata()
        
        