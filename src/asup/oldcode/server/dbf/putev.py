#!/usr/bin/python
# -*- coding: utf-8  -*-'''
'''Считываем данные из putev.dbf и переносить по таблицам в ORM'''
from readdbf import Dbf

class DbfPutev(Dbf):
    'Класс для сравнения путевых листов'
    def _sql(self, params):
        'Переопределяем абстрактный метод'
        dtb = params[0]
        dte = params[1]
        sql = '''SELECT tdata, nompl, garnm, dvyez, dvozv, smena, vremd, vremp,
                prsm1, prsm2, tabv1, tabv2, tabv3, tabv4, marh1, marh2, marh3, marh4,
                 CTOD(RIGHT(dvyez,2)+'/' + RIGHT(tdata,2)+'/' + LEFT(tdata,4))
             FROM putev
             WHERE CTOD(RIGHT(tdata,2)+ '/' + RIGHT(dvyez,2) + '/' + LEFT(tdata,4)) >=  DATETIME(%d, %d, %d, %d, %d)
                 AND CTOD(RIGHT(tdata,2)+ '/' + RIGHT(dvyez,2) + '/' + LEFT(tdata,4)) <=  DATETIME(%d, %d, %d, %d, %d)
             '''%(dtb.year, dtb.month, dtb.day, dtb.hour, dtb.minute, 
                  dte.year, dte.month, dte.day, dte.hour, dte.minute)
        
        return sql
    
    def _file(self):
        return 'putev.dbf'
    

class ReadDbfPutev():
    'Класс чтения из таблицы'
    _path = '\\\\10.1.12.21\\C\\PUT_VYP\\DBF'
    def __init__(self):
        'определяем класс и подключаемся к таблице'
        self.dbfputev = DbfPutev()
        
    def readdata(self, begindate, enddate):
        'Читаем данные из dbf и забрасываем их в список словарей с нормaльными ключами'
        self.dbfputev.connect(self._path)
        datadbf = self.dbfputev.select((begindate, enddate))
        data = []
        print 'len(datadbf)', len(datadbf)
        for row in datadbf:
            tmp = {}
            tmp['nompl']    = int(row[1].strip())
            try:
                tmp['dtviezd']    = self.dbfputev.getdatetime(row[0], row[3].strip(), row[6])
                tmp['dtvozvr']    = self.dbfputev.getdatetime(row[0], row[4].strip(), row[7])
            except Exception, e:
                print "error in putev #", tmp['nompl']
                raise e
            tmp['garnm']    = row[2]
            tmp['smena']    = row[5]
            #tmp['peresmbegin'] = row[8] #TODO: здесь не обрабатываю данные, не знаю вообще зачем мне они
            #tmp['peresmend'] = row[9]
            tmp['tab1']     = row[10]
            tmp['tab2']     = row[11]
            tmp['tab3']     = row[12]
            tmp['tab4']     = row[13]
            tmp['marsh1']   = row[14].strip().decode('cp866')
            tmp['marsh2']   = row[15].strip().decode('cp866')
            tmp['marsh3']   = row[16].strip().decode('cp866')
            tmp['marsh4']   = row[17].strip().decode('cp866')
            data += [tmp]
        self.dbfputev.close()        
        return data
    
    def checktable(self):
        'Проверка даты времени изменения файла'
        self.dbfputev.connect(self._path)        
        data = self.dbfputev.checktimefile()
        self.dbfputev.close()        
        return data