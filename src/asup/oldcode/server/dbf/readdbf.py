#!/usr/bin/python
# -*- coding: utf-8  -*-
try:
    import dbi, odbc
except:
    print 'not found dbi, odbc'
import os, time
from datetime import date, datetime, timedelta
from MySQLdb.times import TimeDelta

class Dbf():
    '''Абстрактный класс для подключения к дбф файлам'''
    cursor = None
    db = None    #ссылка на подключение
    ctime = None #текущее время проверки файла
    _pathdbf = ''#путь к таблицам
    
    def connect(self, path = 'C:\\DBF', dsn = None):
        'Метод подключения к таблице'
        self._pathdbf = path + '\\'
        if not dsn:
            dsn = '''DRIVER={Microsoft Visual FoxPro Driver};
             SourceDB=%s;SourceType=DBF;
             Exclusive=Yes;Deleted=Yes;
             Collate=Machine;NULL=NO;BackGorundFetch=No'''%(path)
        try:
            self.db = odbc.odbc(dsn)
            self.cursor = self.db.cursor()
        except Exception, e:
            print e.args[0].decode("cp1251")
            raise e
    
    def _sql(self, params):
        'Необходимо переопределить для запросов'
        raise Exception("need return in Dbf._sql")
    
    def _file(self):
        'Необходимо переопределить - таблицу которую смортим на изменения'
        raise Exception("need return in Dbf._file")
    
    def select(self, params = None):
        'Выбор данных с возвращением данных, что получили'
        rows = None
        self._params = params #запоминаем на случай изменения в файле
        if self.cursor:
            try:
                if params :
                    self.cursor.execute(self._sql(params))
                else:
                    self.cursor.execute(self._sql())
                rows = self.cursor.fetchall()
                
            except:
                rows = []
            self.ctime = time.ctime(os.path.getmtime(self._pathdbf + self._file()))
        return rows    
        
    def execute(self, params = None):
        'Выполнение запроса без возвращения параметров'
        self._params = params #запоминаем на случай изменения в файле
        if self.cursor:
            if params :
                self.cursor.execute(self._sql(params))
            else:
                self.cursor.execute(self._sql())
    
    def close(self):
        'Закрываем соединение'
        self.db.close()
    
    def checktimefile(self):
        'Проверяем, изменился ли файл, если изменился, выполняем селект и возвращаем его данные'
        if not self.ctime:
            self.ctime = time.ctime(os.path.getmtime(self._pathdbf + self._file()))
        else:
            tmptime = time.ctime(os.path.getmtime(self._pathdbf + self._file()))
            #print 'checktimefile', tmptime, self.ctime, self._pathdbf + self._file()  
            if self.ctime != tmptime:
                self.ctime = tmptime
                return True
        return False
    
    def getdate(self, tdata, day):
        'Переводим год месяц и месяц день в вид date'
        year = int(tdata[:4])
        month = int(tdata[4:])
        try:
            day = int(day[:2])
        except:
            day = 1
        dt = date(year, month, day)
        return dt
    
    def getdatetime(self, tdata, tday, vrem):
        'Переводим год месяц и месяц день в вид date'
        vrem = vrem * 1.0
        year = int(tdata[:4])
        if len(tday):
            day = int(tday[2:])
            month = int(tday[:2])
        else:
            return None
        hour = int(vrem)
        min = int(round(100*(vrem - hour)))
        addday = 0
        if hour == 24:
            addday = 1
            hour =0
        dt = datetime(year, month, day, hour, min, 0) + timedelta(addday,0) 
        return dt