import unittest
from engine.selectcomponent import TestSelectComponent
#from engine.ddjr.modeldictionary import UnitTestModelDictionary

#from client.mehanik.models import TestModelMehanik, TestConnectModelMehanik
from client.mehanik.tests.viewstest import TestView

from tests.ddjrmodel import TestDDJRModel
from tests.ormmodel import TestOrmModel
from tests.authorize import TestAuthorize
from engine.dictionary.tests.bus import TestBusModel
from engine.document.tests.planto12 import TestPlanTO12Model
from engine.journal.tests.to12 import TestTO12Model
from models.tests.shkiper import TestShkiper

if __name__ == "__main__":
    
    testunits = [#TestSelectComponent,
                 #TestAuthorize, 
 #                UnitTestModelDictionary,
                 #TestDDJRModel,
                 #TestConnectModelMehanik,
                 #TestModelMehanik,
                 #TestView, 
                 #TestOrmModel,
                 TestBusModel,
                 TestPlanTO12Model,
                 TestTO12Model,
                 TestShkiper,
                 ]
    
    suiteall = unittest.TestSuite()
    for testunit in testunits:
        suite = unittest.TestLoader().loadTestsFromTestCase(testunit)
        suiteall.addTests(suite)
    unittest.TextTestRunner(verbosity=2).run(suiteall)
    