# -*- coding: utf-8  -*-
class Observer():
    _observers = []
    def appendobserver(self, observer):
        self._observers.append(observer)
    def removeobserver(self, observer):
        self._observers.remove(observer)
    def notifyobservers(self):
        for observer in self._observers:
            observer.modelischanged()

