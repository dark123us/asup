# -*- coding: utf-8  -*-'''
import logging
import os
from configobj import ConfigObj

class Log():
    'Класс настройки логгирования'
    _path = ''
    def __init__(self, path = ''):
        self._path = path
        
        configPath = os.getcwd() + self._path + '/config.ini'
        config = ConfigObj(configPath, encoding='UTF8')
        config.interpolation = False
        _format = config['LOGGING']['format']
        level = config['LOGGING']['level']
        filename = config['LOGGING']['filename']
        if filename:
            logging.basicConfig(format = _format, level = level, filename = filename)
        else:
            logging.basicConfig(format = _format, level = level)
    