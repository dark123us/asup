#!/usr/bin/python
# -*- coding: UTF-8  -*-
from engine.journal.model import ModelJournal
from engine.journal.view import ViewJournal
from engine.journal.control import ControlJournal
from engine.document.planto12 import PlanTo12
from engine.document.readyto12 import ReadyTo12

class To12Model(ModelJournal):
    def __init__(self):
        self._model = [PlanTo12(), ReadyTo12()] 

class To12View(ViewJournal):
    @property
    def header(self):
        return [u'Дата', u'Номер', u'Документ']
    def title(self):
        return u'Журнал ТО-1 ТО-2'
        
class To12(ControlJournal):
    def createmodel(self):
        return To12Model()
    def createview(self, model, parent):
        return To12View(model, parent)
