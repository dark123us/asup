#!/usr/bin/python
# -*- coding: UTF-8  -*-
from PyQt4 import QtGui, QtCore
from engine.widget.view import ViewTable

class ViewJournal(QtGui.QWidget):
    def __init__(self, model, parent = None):
        QtGui.QWidget.__init__(self, parent)
        self._model = model
        self._parent = parent
        self.initvalue()
        self.initwidget()
    @property
    def header(self):
        return []
    @property
    def model(self):
        return self._model
    def initvalue(self):
        self._inittoolbar()
        self._inittable()
        self._dialogselect = ViewJournalSelectDoc() 
        self.connect(self._dialogselect, QtCore.SIGNAL("accepted()"), self._opendoc)
    def _inittoolbar(self):
        self._tbar = QtGui.QToolBar()
        val = [[u'Добавить', 'add.png', self._add],
               [u'Изменить', 'edit.png', self._edit],
               [u'Удалить', 'del.png', self._del],
               ]
        path = 'templates/img/'
        for row in val:
            act = QtGui.QAction(row[0], self)
            self.connect(act, QtCore.SIGNAL("triggered()"), row[2])
            act.setStatusTip(row[0])
            act.setIcon(QtGui.QIcon("{0}{1}".format(path,row[1])))
            self._tbar.addAction(act)
    def _inittable(self):
        self._table = ViewTable()
        self._table.setheader(self.header)
    def initwidget(self):
        gr = QtGui.QGridLayout()
        gr.addWidget(self._tbar)
        gr.addWidget(self._table)
        self.setLayout(gr)
    def redraw(self, docs):
        ''
    def title(self):
        return u'Журнал'
    def _add(self):
        if len(self.model.model) == 1:
            doc = self.model.model[0]
            rec = doc.newdocument()
            wind = doc.showdocument()
            self._parent.setsubwindow(wind)
        else:
            self._dialogselect.sel.clear()
            self._seldoc = self.model.model
            d = []
            for doc in self._seldoc:
                d += [doc.view.title()]
            self._dialogselect.sel.addItems(d)
            self._dialogselect.show()
    def _opendoc(self):
        row = self._dialogselect.sel.currentRow()
        doc = self._seldoc[row]
        rec = doc.newdocument()
        wind = doc.showdocument()
        self._parent.setsubwindow(wind)
        
    def _edit(self):
        ''
    def _del(self):
        ''

class ViewJournalSelectDoc(QtGui.QDialog):
    def __init__(self, parent = None):
        QtGui.QDialog.__init__(self, parent)
        self.sel = QtGui.QListWidget()
        gr = QtGui.QGridLayout()
        gr.addWidget(self.sel)
        btnbox = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok |
                                        QtGui.QDialogButtonBox.Cancel)
        self.connect(btnbox, QtCore.SIGNAL('accepted()'), self.accept)
        self.connect(btnbox, QtCore.SIGNAL('rejected()'), self.reject)
        gr.addWidget(btnbox)
        self.setLayout(gr)
        self.setModal(True)
     