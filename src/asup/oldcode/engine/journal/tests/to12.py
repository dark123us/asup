# -*- coding: utf-8  -*-

#===============================================================================
# Тестовый юнитs        
#===============================================================================
import unittest, threading, socket
from models.orm import (DBSession, Base, Bus)
from engine.dictionary.bus import BusModel
from sqlalchemy import create_engine
import transaction
from sqlalchemy.types import (Integer, String, Time, Date)
from engine.document.planto12 import PlanTo12
from engine.document.readyto12 import ReadyTo12
from engine.journal.to12 import To12Model 
from datetime import datetime, timedelta
from models.orm import (DocumentServiceTo1To2,
                        DocumentReadyServiceTo1To2,)

class TestTO12Model(unittest.TestCase):
    def setUp(self):
        engine = create_engine('sqlite://')
        DBSession.configure(bind=engine)
        Base.metadata.create_all(engine)
    
    def tearDown(self):
        DBSession.remove()
        
    def test_adddocopendoc(self):
        doc1 = PlanTo12()
        doc2 = ReadyTo12()
        newdoc = doc1.newdocument()
        newdoc.docnumber = '1'
        doc1.savedocument()
##        newdoc = doc2.newdocument()
##        newdoc.docnumber = '001'
##        doc2.savedocument()
##        journal = To12Model()
##
##        self.assertIsInstance(journal.model[0], PlanTo12)
##        self.assertIsInstance(journal.model[1], ReadyTo12)
##        docs = journal.getdocs()
##        self.assertIsInstance(docs[0], DocumentServiceTo1To2)
##        self.assertIsInstance(docs[1], DocumentReadyServiceTo1To2)
##        docum = journal.opendoc(docs[1]) 
#        self.assertIsInstance(docum, ReadyTo12)
                
        
    