#!/usr/bin/python
# -*- coding: UTF-8  -*-
class ModelJournal():
    def getdocs(self, begindt = None, enddt = None):
        docs = []
        for model in self.model:
            docs += model.getdocuments(begindt, enddt)
        return docs
    @property
    def model(self):
        return self._model
    def opendoc(self, rec):
        for model in self.model:
            #print (model.model.modelorm(), type(rec))
            if model.model.modelorm() == type(rec):
                return model
    
         