#!/usr/bin/python
# -*- coding: UTF-8  -*-
from PyQt4 import QtGui

class ControlJournal():
    _model = None
    _view = None
    def __init__(self, parent = None):
        self._parent = parent
    @property
    def model(self):
        if self._model is None:
            self._model = self.createmodel()
        return self._model
    @property
    def view(self):
        if self._view is None:
            self._view = self.createview(self.model, self._parent)
        return self._view
    
    def _subwindow(self):
        wind = QtGui.QMdiSubWindow()
        wind.setWidget(self.view)
        wind.setWindowTitle(self.view.title())
        return wind
    
    def getwindow(self):
        docs = self.model.getdocs()
        self.view.redraw(docs)
        return self._subwindow()
        
