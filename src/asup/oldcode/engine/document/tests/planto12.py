# -*- coding: utf-8  -*-

#===============================================================================
# Тестовый юнитs        
#===============================================================================
import unittest, threading, socket
from models.orm import (DBSession, Base, Bus, DocumentServiceTo1To2, User)
from engine.dictionary.bus import BusModel, Bus as BusControl
from sqlalchemy import create_engine
import transaction
from sqlalchemy.types import (Integer, String, Time, Date)
from engine.document.planto12 import PlanTo12Model
from datetime import datetime, timedelta, date

class TestPlanTO12Model(unittest.TestCase):
    def setUp(self):
        engine = create_engine('sqlite://')
        DBSession.configure(bind=engine)
        Base.metadata.create_all(engine)
    
    def tearDown(self):
        DBSession.remove()
    
    def test_autonumber(self):
        doc = PlanTo12Model()
        self.assertEqual(doc._crautonumber('0000001'), '0000002')
        self.assertEqual(doc._crautonumber('0000001a'), '0000002a')
        self.assertEqual(doc._crautonumber('a0000001a'), 'a0000002a')
        self.assertEqual(doc._crautonumber('a0000001'), 'a0000002')
        self.assertEqual(doc._crautonumber('aasdfa'), 'aasdfa')
        self.assertEqual(doc._crautonumber('aa23sd23423fa'), 'aa23sd23424fa') 
        self.assertEqual(doc._crautonumber('aa23sd999fa'), 'aa23sd001fa')
        self.assertEqual(doc._crautonumber('aa23sd23423fa21312s'), 'aa23sd23424fa')
        self.assertEqual(doc._crautonumber(''), '')
    
    def test_newdoc(self):
        user = User()
        doc = PlanTo12Model(user)
        doc.new()
        self.assertEqual(doc.docnumber, '0000001')
        row = doc.newrow()
        bus = Bus()
        bus.garnm = 123
        with transaction.manager:
            DBSession.add(bus)
        row[0].value = bus
        v = row[1] 
        v.value = date.today()
        rows = row[:]
        self.assertEqual(rows[0].value.id, 1)        
        self.assertEqual(row._rec.bus.id, 1)
        self.assertEqual(row['bus'].value.id,1)
        self.assertEqual(row['bus'].relation, BusControl)
        self.assertEqual(doc.table.rows[0][0].value.id,1)
        r = row[1]
        self.assertEqual([r.type, r.name, r.field, r.value, r.relation],
                         ['date', u'Планируемая дата', 'dt', date.today(), None])
        r = row[0]
        self.assertEqual([r.type, r.name, r.field, r.value, r.relation],
                         ['relation', u'Автобус', 'bus', bus, BusControl])
        
        doc.table.delcurrow()
        self.assertEqual(len(doc.table.rows), 0)
        self.assertEqual(doc.rowcount(), 0)
        doc.newrow()
        self.assertEqual(doc.rowcount(), 1)
        doc.save()
        doc.new()
        self.assertEqual(doc.docnumber, '0000002')
    
    def test_delrows(self):
        user = User()
        doc = PlanTo12Model(user)
        doc.new()
        doc.newrow()
        doc.newrow()
        self.assertEqual(doc.rowcount(),2)
        doc.table.delrows()
        self.assertEqual(doc.rowcount(),0)
        row = doc.newrow()
        row1 = doc.newrow()
        self.assertEqual(doc.table._currow, row1)
        doc.table.delcurrow()
        self.assertEqual(doc.table._currow, row)
    
    def test_newsave(self):
        user = User()
        doc = PlanTo12Model(user)
        doc.new()
        doc.save()
        doc.new()
        doc.save()
        doc.new()
        self.assertEqual(len(doc.getdocuments()),2)
        doc.savecancel()
        self.assertEqual(len(doc.getdocuments()),2)
        doc.new()
        doc.newrow()
        doc.newrow()
        self.assertEqual(doc.table.rowcount(), 2)
        doc.savecancel()
        self.assertEqual(doc.table.rowcount(), 0)
        
    def test_open(self):
        user = User()
        bus1 = Bus()
        bus1.garnm = 123
        bus2 = Bus()
        bus2.garnm = 124
        bus3 = Bus()
        bus3.garnm = 125
        with transaction.manager:
            DBSession.add(bus1)
            DBSession.add(bus2)
            DBSession.add(bus3)
        doc = PlanTo12Model(user)
        doc.new()
        doc.save()
        doc2 = doc.new()
        doc.newrow()
        doc.newrow()
        doc.table.rows[0]['bus'].value = bus1
        doc.table.rows[1]['bus'].value = bus2
        doc.save()
        docs = doc.getdocuments()
        self.assertEqual(docs[1].docnumber, '0000002')
        self.assertEqual(doc2.docnumber, '0000002')
        doc22 = doc.open(number = docs[1].docnumber)
        self.assertEqual(doc22.docnumber, '0000002')
        self.assertEqual(doc.rowcount(), 2)
        doc.newrow()
        doc.table.rows[2]['bus'].value = bus3
        self.assertEqual(doc.rowcount(), 3)
        doc.savecancel()
        
        docs = doc.getdocuments()
        doc22 = doc.open(number = docs[1].docnumber)
        self.assertEqual(doc.rowcount(), 2)
        self.assertEqual(doc.table.rows[0]['bus'].value.garnm, 123)
        self.assertEqual(doc.table.rows[1]['bus'].value.garnm, 124)
    
class TestPlanTO12Control(unittest.TestCase):
    def setUp(self):
        engine = create_engine('sqlite://')
        DBSession.configure(bind=engine)
        Base.metadata.create_all(engine)
    
    def tearDown(self):
        DBSession.remove()
    
