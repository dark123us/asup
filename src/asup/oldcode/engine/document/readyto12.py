# -*- coding: utf-8  -*-
from engine.document.model import ModelDocument
from engine.document.view import ViewDocument
from engine.document.control import ControlDocument
from models.orm import DocumentReadyServiceTo1To2, DocTableReadyServiceTo1To2

class ReadyTo12Model(ModelDocument):
    def modelorm(self):
        return DocumentReadyServiceTo1To2
    def modeltableorm(self):
        return DocTableReadyServiceTo1To2

class ReadyTo12View(ViewDocument):
    def title(self):
        return u'Проведение ТО-1 ТО-2'

class ReadyTo12(ControlDocument):
    def createmodel(self):
        return ReadyTo12Model()
    def createview(self, model):
        return ReadyTo12View(model)