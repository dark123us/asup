# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from datetime import datetime

class ControlDocument():
    _model = None
    _view = None
     
    def _subwindow(self):
        wind = QtGui.QMdiSubWindow()
        wind.setWidget(self.view)
        wind.setWindowTitle(self.view.title())
        wind.setBaseSize(500,600)
        return wind
    def createmodel(self):
        raise Exception('need initilazed createmodel')
    def createview(self):
        raise Exception('need initilazed createview')
    @property
    def model(self):
        if self._model is None:
            self._model = self.createmodel()
        return self._model
    @property
    def view(self):
        if self._view is None:
            self._view = self.createview(self.model)
        return self._view
    def newdocument(self):
        'Создаем новый документ'
        return self.model.new()
    def opendocument(self, number = None, rec = None):
        'Открываем документ'
        if rec:
            return self.model.open(rec)
        else:
            return self.model.open(number)
    def showdocument(self):
        data = {'docnumber':self.model.docnumber}
        data["docdate"] = datetime.now()
        self.view.setvalue(data)
        return self._subwindow()
        #self.view.show()
    def savedocument(self):
        self.model.save()
    def openbased(self, doc = None):
        if doc is None:
            return self.newdocument()
    def getdocuments(self, begindt = None, enddt = None):
        return self.model.getdocuments(begindt, enddt)
    
    