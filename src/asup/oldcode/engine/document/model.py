# -*- coding: utf-8  -*-
from models.orm import DBSession
from sqlalchemy import func
import transaction
import re
from datetime import datetime

class ModelRecRow(object):
    def __init__(self, rec):
        self._rec = rec
    @property
    def type(self):
        return self._rec["type"]
    @property
    def name(self):
        return self._rec["namefield"]
    @property
    def field(self):
        return self._rec["field"]
    @property
    def value(self):
        if self.field in self._rec["value"].__dict__:
            return self._rec["value"].__dict__[self.field]
        else:
            #print self._rec["value"]
            #print 
            getattr(self._rec["value"], self.field)
            return self._rec["value"].__dict__[self.field]
    @value.setter
    def value(self, value):
        setattr(self._rec["value"], self.field, value)
        #self._rec["value"].__dict__[self.field] = value
        #DBSession.add(self._rec["value"])
    @property
    def relation(self):
        if 'relation' in self._rec:
            return self._rec["relation"]

class ModelRowTable():
    def __init__(self, doc, rec = None):
        if rec is None:
            self._rec = self.modelorm(doc)
        else:
            self._rec = rec
    def __len__(self):
        return len(self.fields)
    def __getitem__(self, column):
        if self._rec is None:
            return None
        if type(column) == slice:
            rows = []
            for col in range(len(self.fields)):
                rows.append(self[col])
            return rows
        if type(column) == str or type(column) == unicode:
            if column in self.fields:
                return self[self.fields.index(column)]
        rec = {}
        field = self.fields[column]
        rec["field"] = field
        rec["namefield"] = self.namefield(field)
        rec["type"] = self.typefield(field)
        rec["value"] = self._rec
        if rec["type"] == 'relation':
            rec['relation'] = self.relation(field)
        return ModelRecRow(rec)
    @property
    def rec(self):
        return self._rec
    @property
    def modelorm(self):
        'Возвращем ORM для работы с таблицей документа'
        raise Exception('not initilazed modeltableorm')
    @property
    def fields(self):
        return filter(lambda x: not '_' in x, self.modeltableorm().__dict__)
    def getdata(self, modeltable):
        rectable = DBSession.query(self.modelorm).filter(self.modelorm.doc == modeltable).all()
        return rectable
    @property
    def _namefield(self):
        return {}
    @property
    def _relation(self):
        return {}
    def namefield(self, namefield):
        if namefield in self._namefield:
            return self._namefield[namefield]
        else:
            return namefield
    def typefield(self, namefield):
        try:
            return self.modelorm.__dict__[namefield].property.columns[0].type.__visit_name__
        except AttributeError, e:
            return 'relation'
    def relation(self, field):
        if field in self._relation:
            return self._relation[field]
    def delete(self):
        with transaction.manager:
            try:
                DBSession.delete(self._rec)
                self._rec = None
            except:
                pass

class ModelTableDocument():
    def __init__(self, doc):
        self._currow = None
        self._rows = []
        self._doc = doc
        modelorm = self.model(doc).modelorm
        query = DBSession.query(modelorm).all()#.filter(modelorm.docid == doc.id).all()
        for row in query:
            tmp = self.model(doc, row)
            self._rows.append(tmp)
            self._currow = tmp
    @property
    def model(self):
        return ModelRowTable
    @property
    def rows(self):
        return self._rows
    @property
    def currentrow(self):
        return self._currow
    def newrow(self):
        self._currow = self.model(self._doc)
        self._rows.append(self._currow)
        return self._currow
    def rowcount(self):
        return len(self._rows)

    def _setcurrow(self, ind):
        if len(self._rows) > 0:
            if ind >= len(self._rows):
                self._currow = self._rows[len(self._rows)-1]
            else:
                self._currow = self._rows[ind]
    def delcurrow(self):
        ind = self._rows.index(self._currow)
        self._rows.remove(self._currow)
        self._currow.delete()
        self._currow = None
        self._setcurrow(ind)
        
    def delrow(self, row):
        ind = self._rows.index(self._currow)
        self._rows[row].delete()
        self._rows.remove(self._rows[row])
        self._currow = None
        self._setcurrow(ind)
    def delrows(self):
        for row in self._rows:
            row.delete()
        self._rows = []
        self._currow = None

class ModelDocument(object):
    def __init__(self, author = None):
        self._author = author
        self._table = None
        self._curdoc = None
        self._reg = re.compile(r'(\D*)(\d*)(\D*)(\d*)(\D*)')
        self._new = True
        
    @property
    def curdoc(self):
        return self._curdoc
    @property
    def table(self):
        if self._table is None:
            self._table = self.tablemodelorm(self.curdoc)
        return self._table
    @property
    def tablemodelorm(self):
        'Возвращем ORM для работы с таблицей документа документом'
        return ModelTableDocument
    def modelorm(self):
        'Возвращем ORM для работы с документом'
        raise Exception('not initilazed modelorm')
    @property
    def docnumber(self):
        return self.curdoc.docnumber
    @property
    def fields(self):
        return filter(lambda x: not '_' in x, self.modelorm.__dict__)
    def namefield(self, namefield):
        if namefield in self._namefield:
            return self._namefield[namefield]
        else:
            return namefield
    def typefield(self, namefield):
        try:
            return self.modelorm.__dict__[namefield].property.columns[0].type.__visit_name__
        except AttributeError:
            return 'relation'
    def value(self, fieldname):
        return self.curdoc.__dict__[fieldname]
    def new(self):
        self._curdoc = None
        self._table = None
        with transaction.manager:
            number = self.autonumber()
            self._curdoc = self.modelorm()
            self._curdoc.docnumber = number 
            self._curdoc.author = None
            self._curdoc.docdate = datetime.now()
            self._new = True
            DBSession.add(self._curdoc)
        return self._curdoc
    def newrow(self):
        self._needsave = True
        return self.table.newrow()
    def rowcount(self):
        return self.table.rowcount()
    def open(self, rec = None, number = None):
        self._new = False
        if rec is None:
            model = self.modelorm
            self._curdoc = DBSession.query(model).filter(model.docnumber == number).first()
        else:
            self._curdoc = rec
        if self._curdoc is None:
            self.new()
        self._table = None
        return self._curdoc
    def save(self):
        if self._curdoc:
            with transaction.manager:
                self._curdoc.author = self._author
                DBSession.add(self._curdoc)
                for row in self.table.rows:
                    DBSession.add(row.rec)
        self._new = False
    def savecancel(self):
        if self._new: 
            if self._curdoc:
                with transaction.manager:
                    DBSession.delete(self._curdoc)
        self._curdoc = None
        self._table = None
        self._new = False
    def edit(self, datadict):
        if self._curdoc:
            with transaction.manager:
                for key in datadict:
                    self._curdoc.__dict__[key] = datadict[key] 
                DBSession.add(self._curdoc)
        return self._curdoc
    def delete(self, rec):
        with transaction.manager:
            DBSession.delete(rec)
    def getdocuments(self, docbegindt = None, docenddt = None):
        model = self.modelorm
        if docbegindt is None and docenddt is None:
            recs = DBSession.query(model).filter(model.author != None).all()
        elif docbegindt is None:
            recs = DBSession.query(model).filter(model.author != None)\
                                         .filter(model.docdate <= docenddt).all()
        elif docenddt is None:
            recs = DBSession.query(model).filter(model.author != None)\
                                         .filter(model.docdate >= docbegindt).all()
        else:
            recs = DBSession.query(model).filter(model.docdate >= docbegindt)\
                                         .filter(model.author != None)\
                                         .filter(model.docdate <= docenddt)\
                                         .all()
        return recs
    
    def autonumber(self):
        model = self.modelorm
        dt = DBSession.query(func.max(model.docdate)).first()
        dt = dt[0]
        if dt:
            rec = DBSession.query(model).filter(model.docdate == dt).first()
            if rec:
                numb = self._crautonumber(rec.docnumber)
                return numb
        return '0000001'
            
    
    def _crautonumber(self, number):
        def retnumber(num):
            n = "{0}".format(int(num)+1)
            if len(n) > len(num):
                n = "{0}1".format('0'*(len(num)-1))
            if len(n) < len(num):
                n = "{0}{1}".format('0'*(len(num)-len(n)), n)
            return n
        res = self._reg.match(number)
        val = res.groups()
        if val[3]:
            res = "{0}{1}{2}{3}{4}".format(val[0], val[1], val[2], retnumber(val[3]), val[4])
        elif val[1]:
            res = "{0}{1}{2}{3}{4}".format(val[0], retnumber(val[1]), val[2], val[3], val[4])
        else:
            res = number 
        return res
    
    
