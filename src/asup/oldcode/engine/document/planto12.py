# -*- coding: utf-8  -*-
from engine.document.model import ModelDocument, ModelTableDocument, ModelRowTable
from engine.document.view import ViewDocument
from engine.document.control import ControlDocument
from models.orm import DocumentServiceTo1To2, DocTableServiceTo1To2
from engine.dictionary.bus import Bus


class PlanTo12RowModel(ModelRowTable):
    @property
    def _namefield(self):
        return {'bus': u'Автобус', 
               'dt': u'Планируемая дата', 
               'typeservice': u'Вид обслуживания'}
    @property
    def _relation(self):
        return {'bus': Bus, 
               'typeservice': None}
    @property
    def modelorm(self):
        return DocTableServiceTo1To2
    @property
    def fields(self):
        return ['bus', 'dt', 'typeservice']
    

class PlanTo12TableModel(ModelTableDocument):
    @property
    def model(self):
        return PlanTo12RowModel

class PlanTo12Model(ModelDocument):
    @property
    def _namefield(self):
        return {'docnumber': u'Номер документа',
                'docdate': u'Дата документа'}
    @property    
    def modelorm(self):
        return DocumentServiceTo1To2
    @property    
    def tablemodelorm(self):
        return PlanTo12TableModel 

class PlanTo12View(ViewDocument):
    def title(self):
        return u'Планирование ТО-1 ТО-2'

class PlanTo12(ControlDocument):
    def createmodel(self):
        return PlanTo12Model()
    def createview(self, model):
        return PlanTo12View(model)