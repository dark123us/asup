# -*- coding: utf-8  -*-
from PyQt4 import QtGui, QtCore
from engine.widget.view import (ViewTable,
                                 ViewWidget)
 
class ViewDocument(QtGui.QWidget):
    _viewdata = {}
    _defwidget = {}
    def __init__(self, model, parent = None):
        self._model = model
        QtGui.QWidget.__init__(self, parent)
        self.initvalue()
        self.initwidget()
    @property
    def model(self):
        return self._model
    
    def _inittoolbar(self):
        self._tbar = QtGui.QToolBar()
        val = [[u'Добавить', 'add.png', self._addrow],
               [u'Изменить', 'edit.png', self._editrow],
               [u'Удалить', 'del.png', self._delrow],
               ]
        path = 'templates/img/'
        for row in val:
            act = QtGui.QAction(row[0], self)
            self.connect(act, QtCore.SIGNAL("triggered()"), row[2])
            act.setStatusTip(row[0])
            act.setIcon(QtGui.QIcon("{0}{1}".format(path,row[1])))
            self._tbar.addAction(act)
    
    def _initdefaultwidget(self):
        l = QtGui.QGridLayout()
        h = QtGui.QHBoxLayout()
        l.addLayout(h,0,0,1,2)
        fields = self.model.fields
        row = 1
        self._defwidget = {}
        for field in fields:
            if not field in ('id', 'userid', 'isheld', 'isdelete'): #исключаем служебные поля
                namefield = self.model.namefield(field)
                typefield = self.model.typefield(field)
                lbl = QtGui.QLabel(namefield)
                if namefield == 'author':
                    widg = ViewWidget().getwidget('label')
                else:
                    widg = ViewWidget().getwidget(typefield)
                    if typefield == 'relation':
                        widg.setrelationwidget(self._viewdata[namefield])
                if field in ('docnumber', 'docdate'):
                    h.addWidget(lbl)
                    h.addWidget(widg)
                else:
                    l.addWidget(lbl,row,0) 
                    l.addWidget(widg,row,1)
                    row += 1
                self._defwidget[field] = widg 
        h.addStretch(1)
        return l
    
    def initvalue(self):
        self._data = []
        self._inittoolbar()
        #self._initeditdialog()
        self._table = ViewTable()
        header = []
        for name in self.model.fields:
            header.append(self.model.namefield(name)) 
        self._table.setheader(header)
        self._table.resizeColumnsToContents()
    
    def initwidget(self):
        gr = QtGui.QGridLayout()
        gr.addWidget(self._tbar)
        l = self._initdefaultwidget()
        gr.addLayout(l, 1,0)
        gr.addWidget(self._table)
        btnbox = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Save |
                                        QtGui.QDialogButtonBox.Cancel)
        self.connect(btnbox, QtCore.SIGNAL('accepted()'), self.close)
        self.connect(btnbox, QtCore.SIGNAL('rejected()'), self.close)
        gr.addWidget(btnbox)
        self.setLayout(gr)
    
    def title(self):
        return u'Документ'
    def _addrow(self):
        self._table.setRowCount(self._table.rowCount()+1)
        self._table.resizeRowsToContents()
        rec = self.model.addrow()
        self._nexteditrow(rec, 0)

    def _editrow(self):
        ''
    def _delrow(self):
        ''
    
    def setvalue(self, datadict):
        for key in datadict:
            self._defwidget[key].setvalue(datadict[key])
    
    def _nexteditrow(self, rec, numberrec):
        typerec = self.model.tabletypefield(self.model.tablefields()[0])
        print typerec