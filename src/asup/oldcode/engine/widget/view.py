#!/usr/bin/python
# -*- coding: UTF-8  -*-
from PyQt4 import QtGui, QtCore, Qt
from datetime import date, datetime

class Table(QtGui.QTableWidget):
    def __init__(self, parent = None):
        QtGui.QTableWidget.__init__(self, parent)
        self._edit = False
        self._enter = True 
        self.initWidget()
    
    def initWidget(self):
        #self.connect(self, QtCore.SIGNAL('cellActivated(int, int)'), self._dropsel)
        self.connect(self, QtCore.SIGNAL('cellClicked(int, int)'), self._clicked)
        self.connect(self, QtCore.SIGNAL('cellEntered()'), self._entered)

    def _dropsel(self, i, j):
        print '_dropsel'
        self._endedit()
        
    def keyReleaseEvent (self, event):
        #ViewJournal.keyReleaseEvent(self, event)
        if event.key() == Qt.Qt.Key_Enter or event.key() == Qt.Qt.Key_Return:
            if self._edit == False:
                if self._enter:
                    self.emit(QtCore.SIGNAL('cellEntered()'))
                    self._enter = False
                else:
                    self._enter = True

    def _gettypeeditwidget(self):
        e = EditWidget()
        e.addItems(['1','2','3','4'])
        e.setCurrentIndex(-1)
        return e

    def _setdefwidget(self, widg):
        self.connect(widg, QtCore.SIGNAL('currentIndexChanged (int)'), self._saveresultwidget)
        widg.showPopup()
    
    def _getvaluewidget(self):
        widg = self.cellWidget(self._row, self._col)
        if widg:
            widg.setVisible(False)
            return widg.itemText(widg.currentIndex())

    def _nextedit(self):
        print '_nextedit'
        if self._edit == False:
            self._edit = True
            self._row = self.currentRow()
            self._col = self.currentColumn()
        else:
            if self._col == self.columnCount()-1:
                self._endedit()
            else:
                self._col += 1
        if self._edit:
            self.setCurrentCell(self._row, self._col)
            widg = self._gettypeeditwidget()
            self.setCellWidget(self._row, self._col, widg)
            self._setdefwidget(widg)
    
    def _saveresultwidget(self):
        print '_saveresultwidget'
        i = self._getvaluewidget()
        if i:
            self.setCellWidget(self._row, self._col, None)
            item = QtGui.QTableWidgetItem()
            item.setText(i)
            self.setItem(self._row, self._col, item)
            if self._edit:
                self._nextedit()
    
    def _endedit(self):
        print '_endedit'
        self.blockSignals(True)
        if self._edit:
            self._edit = False
            self._saveresultwidget()
        self._edit = False
        print '_endedit2'
        self.blockSignals(False)
    
    def _entered(self):
        print '_entered'
        if self._edit:
            self._endedit()
        else:
            self._nextedit()
    
    def _clicked(self, row, col):
        print '_clicked'
        if self._edit:
            self._endedit()
            self.setCurrentCell(row, col)
        else:
            print self.currentRow(), row, self.currentColumn(), col 
            if self.currentRow() == row and self.currentColumn() == col:
                self._nextedit()
            else:
                self.setCurrentCell(row, col)


class EditWidget(QtGui.QComboBox):
    def keyPressEvent (self, event):
        print 12333
        #ViewJournal.keyReleaseEvent(self, event)
        if event.key() == Qt.Qt.Key_Escape:
            print 126
            self.emit(QtCore.SIGNAL('rej()'))
    

class TestWidget(QtGui.QWidget):
    def __init__(self, parent = None):
        QtGui.QWidget.__init__(self, parent)
        self.initvalue()
        self.initwidget()
    
    def initvalue(self):
        self.table = Table()
        self.table.setEditTriggers(QtGui.QAbstractItemView.EditTriggers(0))
        self.table.setRowCount(3)
        self.table.setColumnCount(4)
        self._vw = False
    
    def initwidget(self):
        gr = QtGui.QGridLayout()
        gr.addWidget(self.table)
        self.setLayout(gr)
    
            


        

class ViewTable(QtGui.QTableWidget):
    def setheader(self, header):
        self.setColumnCount(len(header))
        self.setHorizontalHeaderLabels(header)
        
    def redraw(self, data):
        self.setRowCount(len(data))
        for irow, row in enumerate(data):
            for icol, val in enumerate(row):
                self._item(irow, icol, val)
        self.resizeColumnsToContents()
        self.resizeRowsToContents()
    
    def _item(self,row,col,value):
        item = QtGui.QTableWidgetItem()
        item.setText(u"{0}".format(value))
        self.setItem(row, col, item)

class ViewWidget():
#    def __init__(self, parent = None):
#        ''
    def getwidget(self, nametypewidget):
        tmp = None
        if nametypewidget == 'string':
            tmp =  ViewLineEdit()
        elif nametypewidget == 'integer':
            tmp =  ViewSpinEdit()
        elif nametypewidget == 'date': 
            tmp = ViewDateEdit()
        elif nametypewidget == 'datetime': 
            tmp = ViewDateTimeEdit()
        elif nametypewidget == 'label':
            tmp = ViewLabel()
        elif nametypewidget == 'relation':
            tmp = ViewLineEditSelect()
        else:
            tmp = QtGui.QLabel()
        return tmp

class ViewLineEdit(QtGui.QLineEdit):
    def setvalue(self, value):
        self.setText(value)
    def value(self):
        return str(self.text().toUtf8())
    def clearvalue(self):
        self.setText('')        
class ViewSpinEdit(QtGui.QSpinBox):
    def __init__(self, parent = None):
        QtGui.QSpinBox.__init__(self, parent)
        self.setMaximum(1000000)
    def setvalue(self, value):
        QtGui.QSpinBox.setValue(self, value)
    def value(self):
        return QtGui.QSpinBox.value(self)
    def clearvalue(self):
        QtGui.QSpinBox.setValue(self, 0)

class ViewLabel(QtGui.QLabel):
    def setvalue(self, value):
        QtGui.QLabel.setText(value)
    def value(self):
        return QtGui.QLabel.text()
    def clearvalue(self):
        QtGui.QLabel.setText('')

class ViewDateEdit(QtGui.QDateEdit):
    def __init__(self, parent = None):
        QtGui.QDateEdit.__init__(self, parent)
        self.setCalendarPopup(True)
        self.setDate(QtCore.QDate(date.today()))
    def setvalue(self, value):
        self.setDate(value)
    def value(self):
        return self.date().toPyObject()
    def clearvalue(self):
        self.setDate(QtCore.QDate(date.today()))

class ViewDateTimeEdit(QtGui.QDateTimeEdit):
    def __init__(self, parent = None):
        QtGui.QDateTimeEdit.__init__(self, parent)
        self.setCalendarPopup(True)
        self.clearvalue()
    def setvalue(self, value):
        self.setDateTime(value)
    def value(self):
        return self.dateTime().toPyObject()
    def clearvalue(self):
        self.setDateTime(QtCore.QDateTime(datetime.now()))


class ViewLineEditSelect(QtGui.QWidget):
    _value = None
    def __init__(self, parent = None):
        QtGui.QWidget.__init__(self, parent)
        self._line = QtGui.QLineEdit()
        self._line.setEnabled(False)
        self._btn = QtGui.QPushButton('...')
        self.connect(self._btn, QtCore.SIGNAL("clicked()"), self._select)
        hl = QtGui.QHBoxLayout()
        hl.setMargin(0)
        hl.addWidget(self._line)
        hl.addWidget(self._btn)
        self.setLayout(hl)
    def setrelationwidget(self, widget):
        self._widget = widget
        self.connect(self._widget.widgetselect(), QtCore.SIGNAL('accepted()'), self._accept)
    def setvalue(self, value):
        self._value = value
        text = self._widget.valuestr(self._value)
        self._line.setText(text)
    def value(self):
        return self._value
    def clearvalue(self):
        self._line.setText('')
    def _select(self):
        self._widget.widgetselect().redraw()
        self._widget.widgetselect().show()
    def _accept(self):
        self._value = self._widget.widgetselect().value()
        self.setvalue(self._value)