# -*- coding: utf-8  -*-'''
from models.orm import Bus as BusOrm
from engine.dictionary.model import ModelDictionary
from engine.dictionary.view import ViewDictionary, ViewDictionarySelect
from engine.dictionary.marka import Marka 

class BusModel(ModelDictionary):
    _names  = {'id': u'Ключ', 
               #'garnm': u'Гаражный', 
               'fullgarnm' : u'Гаражный',
               'gosnm': u'Гос.номер', 
               'marka': u'Марка'}
    _keys = ['id', 'fullgarnm','gosnm','marka']
    
    def value(self, rec, fieldname):
        'Преобразование значений полей в строки'
        ret = ''
        if rec:
            if fieldname == 'marka':
                if rec.marka:
                    ret = rec.marka.name
            else:
                ret = ModelDictionary.value(self, rec, fieldname)
        return ret
                
    def model(self):
        return BusOrm
    
    def namefield(self, namefield):
        return self._names[namefield]
    
    def fields(self):
        return self._keys

class BusView(ViewDictionary):
    _viewdata = {} #подстановка виджета 
    def __init__(self, parent = None):
        self._model = BusModel()
        self._viewdata['marka'] = Marka()
        ViewDictionary.__init__(self, self._model, parent)
        

class Bus():
    def __init__(self, parent = None):
        self._view = BusView(parent) 
        self._viewselect = ViewDictionarySelect(self._view)
        self._viewselect.setWindowTitle(u'Выбор автобуса')
    def widget(self):
        return self._view
    def title(self):
        return u'Справочник автобусов'
    def redraw(self):
        self._view.redraw()
