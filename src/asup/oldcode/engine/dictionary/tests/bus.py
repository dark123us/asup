# -*- coding: utf-8  -*-

#===============================================================================
# Тестовый юнитs        
#===============================================================================
import unittest, threading, socket
from models.orm import (DBSession, Base, Bus)
from engine.dictionary.bus import BusModel
from sqlalchemy import create_engine
import transaction
from sqlalchemy.types import (Integer, String, Time, Date,
                        
                        )

class TestBusModel(unittest.TestCase):
    def setUp(self):
        engine = create_engine('sqlite://')
        DBSession.configure(bind=engine)
        Base.metadata.create_all(engine)
    
    def tearDown(self):
        DBSession.remove()
        
    def test_data(self):
        with transaction.manager:
            b = Bus()
            b.garnm = '1234'
            b.fullgarnm = '1234'
            DBSession.add(b)
        bus = BusModel()
        a = bus.data()
        for row in a:
            self.assertEqual(row.fullgarnm, 1234) 
    
    def test_add(self):
        tmp = {}
        tmp["fullgarnm"] = 12321
        bus = BusModel()
        bus.add(tmp)
        a = bus.data()
        for row in a:
            self.assertEqual(row.fullgarnm, 12321)

    def test_edit(self):
        tmp = {}
        tmp["fullgarnm"] = 12321
        bus = BusModel()
        v = bus.add(tmp)
        tmp["garnm"] = 121
        bus.edit(v, tmp)
        a = bus.data()
        for row in a:
            self.assertEqual(row.fullgarnm, 12321)
    
    def test_delete(self):
        tmp = {}
        tmp["garnm"] = 12321
        bus = BusModel()
        v = bus.add(tmp)
        bus.delete(v)
        self.assertEqual(bus.data(),[])

    def test_singlenton(self):
        bus = BusModel()
        bus2 = BusModel()
        self.assertEqual(bus, bus2)
    
    def test_observer(self):
        class testobserver():
            init = False
            def modelischanged(self):
                self.init = True
        bus = BusModel()
        bus2 = BusModel()
        tst = testobserver()
        bus.appendobserver(tst)
        bus2.notifyobservers()
        self.assertTrue(tst.init)
    
    def test_header(self):
        bus = BusModel()
        #print Date.__visit_name__, Integer.__visit_name__, String(20).__visit_name__
        self.assertEqual(bus.model().gosnm.property.columns[0].type.__visit_name__, String(20).__visit_name__)
        self.assertEqual(bus.model().garnm.property.columns[0].type.__visit_name__, Integer.__visit_name__)
        self.assertEqual(bus.model().dtvvoda.property.columns[0].type.__visit_name__, Date.__visit_name__)
    
    def test_typefield(self):
        bus = BusModel()
        self.assertEqual(bus.typefield('garnm'),'integer')
        self.assertEqual(bus.typefield('dtvvoda'),'date')
        self.assertEqual(bus.typefield('gosnm'),'string')
        self.assertEqual(bus.typefield('marka'),'relation')
        
    def test_fields(self):
        bus = BusModel()
        a = sorted(bus.fields())
        self.assertEqual(a,['fullgarnm', 'gosnm', 'id', 'marka'])
