#!/usr/bin/python
# -*- coding: UTF-8  -*-
from models.orm import DBSession
import transaction

class Model(object):
    '''Основа для моделей содержит в себе шаблоны синглентон и наблюдатель'''
    _observers = []
    _instance = None
    def __new__(class_, *args, **kwargs):
        if not isinstance(class_._instance, class_):
            class_._instance = object.__new__(class_, *args, **kwargs)
        return class_._instance
    
    def appendobserver(self, observer):
        self._observers.append(observer)
    def removeobserver(self, observer):
        self._observers.remove(observer)
    def notifyobservers(self):
        for observer in self._observers:
            observer.modelischanged()

class ModelDictionary(Model):
    def model(self):
        raise Exception('Need initilazed')
    
    def data(self):
        query = DBSession.query(self.model()).all()
        return query
    
    def value(self, rec, fieldname):
        return rec.__dict__[fieldname]
    
    def datatostr(self, query):
        st = []
        for row in query:
            tmp = []
            for col in self.fields():
                tmp.append(self.value(row, col))
            st.append(tmp)
        return st
    
    def add(self, datadict):
        with transaction.manager:
            value = self.model()()
            for key in datadict:
                value.__dict__[key] = datadict[key]
                if self.typefield(key) == 'relation': 
                    value.__dict__["{0}id".format(key)] = value.__dict__[key].id
            DBSession.add(value)
        self.notifyobservers()
        return value
    
    def edit(self, value, datadict):
        with transaction.manager:
            for key in datadict:
                value.__dict__[key] = datadict[key] 
            DBSession.add(value)
        self.notifyobservers()
        return value
    
    def delete(self, value):
        DBSession.flush()
        with transaction.manager:
            DBSession.delete(value)
        self.notifyobservers()
        
    def typefield(self, namefield):
        try:
            return self.model().__dict__[namefield].property.columns[0].type.__visit_name__
        except AttributeError:
            return 'relation'
    
    def namefield(self, namefield):
        return namefield
    
    def fields(self):
        return filter(lambda x: not '_' in x, self.model().__dict__)
    