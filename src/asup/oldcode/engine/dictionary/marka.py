# -*- coding: utf-8  -*-'''
from models.orm import Marka as MarkaOrm
from engine.dictionary.model import ModelDictionary
from engine.dictionary.view import ViewDictionary, ViewDictionarySelect
from PyQt4 import QtGui, QtCore 

class MarkaModel(ModelDictionary):
    _names  = {'id': u'Ключ', 
               'name': u'Название', 
               'name2': u'Дополнительное название', 
               'crmid': u'Ключ (путевой лист)',
               'crmidosnov': u'Ключ основы (путевой лист)',
               }
    _keys = ['id','name','name2','crmid', 'crmidosnov']
                
    def model(self):
        return MarkaOrm
    
    def namefield(self, namefield):
        return self._names[namefield]
    
    def fields(self):
        return self._keys

class MarkaView(ViewDictionary):
    def __init__(self, parent = None):
        self._model = MarkaModel()
        ViewDictionary.__init__(self, self._model, parent)

class Marka():
    def __init__(self, parent = None):
        self._view = MarkaView(parent)
        self._viewselect = ViewDictionarySelect(self._view)
        self._viewselect.setWindowTitle(u'Выбор марки')
    def widget(self):
        return self._view
    def widgetselect(self):
        return self._viewselect
    def title(self):
        return u'Справочник марок'
    def redraw(self):
        self._view.redraw()
    def valuestr(self, rec):
        return rec.name
            
