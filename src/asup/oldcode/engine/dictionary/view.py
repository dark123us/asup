#!/usr/bin/python
# -*- coding: UTF-8  -*-
from PyQt4 import QtGui, QtCore
from datetime import date, datetime
from engine.widget.view import (ViewTable,
                                 ViewWidget)

class ViewEditDialog(QtGui.QDialog):
    _widgets = {}
    def __init__(self, parent = None):
        QtGui.QDialog.__init__(self, parent)
        self._defgrid = QtGui.QGridLayout()
        self.initwidget()
    
    def initwidget(self):
        grd = QtGui.QGridLayout()
        grd.addLayout(self._defgrid, 0, 0)
        btnbox = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Save |
                                        QtGui.QDialogButtonBox.Cancel)
        self.connect(btnbox, QtCore.SIGNAL('accepted()'), self.accept)
        self.connect(btnbox, QtCore.SIGNAL('rejected()'), self.reject)
        grd.addWidget(btnbox)
        self.setLayout(grd)
    
    def _cleardefgrid(self):
        item = self._defgrid.itemAt(0)
        while item: 
            self._defgrid.removeItem(item)
            self._defgrid.removeWidget(item.widget())
            self._defgrid.update()
            item = self._defgrid.itemAt(0)
    
    def setdefaultwidget(self, dataforwidget):
        '''Устанавливаем по умолчанию виджеты
        Ожидаем список словарей вида имя виджета, его лейбл, тип, значение'''
        self._widgets = {}
        self._cleardefgrid()
        for ind, data in enumerate(dataforwidget):
            lbl = QtGui.QLabel(data['label'])
            widg = ViewWidget().getwidget(data['type'])
            if data['name'] == 'id':
                widg.setEnabled(False)
            if data['type'] == 'relation':
                widg.setrelationwidget(data['relation'])
            self._widgets[data['name']] = widg 
            self._defgrid.addWidget(lbl, ind, 0)
            self._defgrid.addWidget(widg, ind, 1)
    
    def setvalue(self, data):
        for ind, row in enumerate(data):
            self._widgets[row['name']].setvalue(row["value"])
    
    def setaddmode(self, mode):
        self._mode = mode
        if mode:
            for key in self._widgets:
                self._widgets[key].clearvalue()
    
    def addmode(self):
        return self._mode
    
    def value(self):
        value = {}
        for key in self._widgets:
            if key != 'id':
                value[key] = self._widgets[key].value()
        return value
            

class ViewDictionary(QtGui.QWidget):
    _viewdata = {}
    def __init__(self, model, parent = None):
        QtGui.QWidget.__init__(self, parent)
        self._model = model
        self._model.appendobserver(self)
        self.initvalue()
        self.initwidget()
    
    def modelischanged(self):
        self.redraw()

    def _inittoolbar(self):
        self._tbar = QtGui.QToolBar()
        val = [[u'Добавить', 'add.png', self._add],
               [u'Изменить', 'edit.png', self._edit],
               [u'Удалить', 'del.png', self._del],
               ]
        path = 'templates/img/'
        for row in val:
            act = QtGui.QAction(row[0], self)
            self.connect(act, QtCore.SIGNAL("triggered()"), row[2])
            act.setStatusTip(row[0])
            act.setIcon(QtGui.QIcon("{0}{1}".format(path,row[1])))
            self._tbar.addAction(act)
    
    def initvalue(self):
        self._data = []
        self._inittoolbar()
        self._initeditdialog()
        self._table = ViewTable()
        header = []
        for name in self._model.fields():
            header.append(self._model.namefield(name)) 
        self._table.setheader(header)
    
    def initwidget(self):
        gr = QtGui.QGridLayout()
        gr.addWidget(self._tbar)
        gr.addWidget(self._table)
        self.setLayout(gr)
    
    def redraw(self):  
        self._data = self._model.data()
        table = self._model.datatostr(self._data)
        self._table.redraw(table)
    
    def _add(self):
        self._eddlg.setWindowTitle(u'Добавить')
        self._eddlg.setaddmode(True)
        self._eddlg.show()
        
    def _edit(self):
        self._eddlg.setWindowTitle(u'Изменить')
        self._eddlg.setaddmode(False)
        row = self._table.currentRow()
        rec = None
        if row >= 0:
            rec = self._data[row]
            values = []
            for name in self._model.fields():
                tmp ={}
                tmp["name"] = name
                tmp["value"] = rec.__dict__[name]#value(rec, name)
                values.append(tmp)
            self._eddlg.setvalue(values)
        #self._eddlg._widgets[]
        self._eddlg.show()
    
    def _del(self):
        row = self._table.currentRow()
        rec = None
        if row >= 0:
            rec = self._data[row]
            if rec:
                dlg = QtGui.QMessageBox(self)
                dlg.setWindowTitle(u'Удаление записи')
                dlg.setText(u'''Вы действительно хотите удалить запись {0}?'''.format(rec.id))
                dlg.setStandardButtons(QtGui.QMessageBox.Cancel | QtGui.QMessageBox.Ok );
                ok = dlg.exec_()
                if ok==QtGui.QMessageBox.Ok :
                    self._model.delete(rec)
    
    def _initeditdialog(self):
        self._eddlg = ViewEditDialog(self)
        self.connect(self._eddlg, QtCore.SIGNAL("accepted()"), self._save)
        dataforwidget = []
        for row in self._model.fields():
            tmp ={}
            tmp["name"] = row
            tmp["label"] = self._model.namefield(row)
            tmp["type"] = self._model.typefield(row)
            if tmp["type"] == 'relation':
                tmp['relation'] = self._viewdata[row]
            dataforwidget.append(tmp)
        self._eddlg.setdefaultwidget(dataforwidget)
    
    def _save(self):
        value = self._eddlg.value()
        if self._eddlg.addmode():
            self._model.add(value)
        else:
            row = self._table.currentRow()
            rec = None
            if row >= 0:
                rec = self._data[row]
                if rec:
                    self._model.edit(rec, value)
                else:
                    self._model.add(value)
    
    def value(self):
        row = self._table.currentRow()
        rec = self._data[row]
        return rec
    
class ViewDictionarySelect(QtGui.QDialog):
    def __init__(self, view, parent = None):
        QtGui.QDialog.__init__(self, parent)
        self._view = view
        self._initselect()
        
    def _initselect(self):
        gr = QtGui.QGridLayout()
        gr.addWidget(self._view)
        btnbox = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok |
                                        QtGui.QDialogButtonBox.Cancel)
        self.connect(btnbox, QtCore.SIGNAL('accepted()'), self.accept)
        self.connect(btnbox, QtCore.SIGNAL('rejected()'), self.reject)
        gr.addWidget(btnbox)
        self.setLayout(gr)
        self.setModal(True)
    
    def redraw(self):
        self._view.redraw()
    
    def value(self):
        rec = self._view.value()
        return rec
        
