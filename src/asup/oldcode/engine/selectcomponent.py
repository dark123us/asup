# -*- coding: utf-8  -*-'''
import os
from configobj import ConfigObj
import logging
import asyncore, threading, socket
from server.authorize import Authorize
import asyncore
import unittest
from client.mehanik.control import Mehanik
from models.orm import Engine
from client.pto.view import Pto 

log = logging.getLogger(__name__)

class ConnectDatabase():
    def __init__(self, config = None, path = ''):
        if not config:
            configPath = u"{0}{1}{2}".format(os.getcwd(),path,'/config.ini')
            print configPath
            config = ConfigObj(configPath, encoding='UTF8')
        engine  = config['DATABASE']['engine']
        host    = config['DATABASE']['host']
        dbname  = config['DATABASE']['dbname']
        user    = config['DATABASE']['user']
        passwd  = config['DATABASE']['password']
        echo    = config['DATABASE']['echo']
        settings = {}
        if engine == 'mysql+mysqldb':
            settings['sqlalchemy.url'] = '{0}://{1}:{2}@{3}/{4}?charset=utf8'.format(engine,user,passwd, host,dbname)
        else:
            settings['sqlalchemy.url'] = '{0}://{1}:{2}@{3}/{4}'.format(engine,user,passwd, host,dbname)
        settings['sqlalchemy.echo'] = echo
        
        #settings['sqlalchemy.connect_args'] = {'check_same_thread':False}
        engine = Engine(settings)
        engine.configure()
        
class ComponentAuthorize():
    def start(self):
        logging.debug('starting server authorization')
        ConnectDatabase()
        server = Authorize()
        server.startserver()
        try:
            asyncore.loop(1)
        except KeyboardInterrupt:
            server.close()
            logging.debug("KeyboardInterrupt - server stopping")
        except Exception, e:
            logging.debug("{0} {1}".format(type(e), e))
    
    def needauthorize(self):
        'Проверка, требуется ли авторизация для компонента'
        return False

class ComponentClient():
    def start(self, auth):
        'Запускаем работу компонента'
        config = {}
        config['DATABASE'] = {}
        config['DATABASE']['engine']    = auth[0]
        config['DATABASE']['host']      = auth[1]
        config['DATABASE']['dbname']    = auth[2]
        config['DATABASE']['user']      = auth[3]
        config['DATABASE']['password']  = auth[4]
        config['DATABASE']['echo']      = False
        if auth[5] == 1:
            ConnectDatabase(config)
            logging.debug("start mehanik component")
            ComponentMehanik().start(auth)
        elif auth[5] == 2:
            ConnectDatabase(config)
            logging.debug("start pto component")
            ComponentPto().start(auth)
    
    def needauthorize(self):
        return True

class ComponentMehanik():
    def start(self, auth):
        'Запускаем работу компонента'
        Mehanik().start(auth)
    
    def needauthorize(self):
        return True

class ComponentPto():
    def start(self, auth):
        'Запускаем работу компонента'
        Pto().start(auth)
    
    def needauthorize(self):
        return True

class SelectComponent():
    '''Выбираем используемый компонент, определяем, как будем использовать
    нужно ли читать конфигурацию или получить авторизацию, как его настроить
    для запуска и запуск работы компонента'''
    _path = ''
    
    def getcomponent(self, component = -1):
        if component == -1:
            configPath = os.getcwd() + self._path + '/config.ini'
            config = ConfigObj(configPath, encoding='UTF8')
            component = config['MAIN']['component']
        if component == 'authorize' or component == 0:
            logging.debug('using {0} component'.format(component))
            return ComponentAuthorize()
        elif component == 'mehanik' or component == 1:
            logging.debug('using {0} component'.format(component))
            return ComponentMehanik()
        elif component == 'pto' or component == 2:
            logging.debug('using {0} component'.format(component))
            return ComponentPto()
        elif component == 'client':
            logging.debug('using {0} component'.format(component))
            return ComponentClient()
        else:
            logging.debug('component {0} not found '.format(component))
            return None

class TestSelectComponent(unittest.TestCase):
    def test_getcomponent(self):
        component = SelectComponent()
        getcomponent = component.getcomponent(0)
        self.assertEqual(getcomponent.__class__, ComponentAuthorize, u"неверно возвращаемый тип")
        getcomponent = component.getcomponent('authorize')
        self.assertEqual(getcomponent.__class__, ComponentAuthorize, u"неверно возвращаемый тип") 
        getcomponent = component.getcomponent(1)
        self.assertEqual(getcomponent.__class__, ComponentMehanik, u"неверно возвращаемый тип")
        getcomponent = component.getcomponent('mehanik')
        self.assertEqual(getcomponent.__class__, ComponentMehanik, u"неверно возвращаемый тип")
        getcomponent = component.getcomponent('pto')
        self.assertEqual(getcomponent.__class__, ComponentPto, u"неверно возвращаемый тип")
        getcomponent = component.getcomponent(3)
        self.assertIsNone(getcomponent, u"неверно возвращаемый тип")

#if __name__ == "__main__":
#    #unittest.main()
#    suite = unittest.TestLoader().loadTestsFromTestCase(TestSelectComponent)
#    unittest.TextTestRunner(verbosity=2).run(suite)
