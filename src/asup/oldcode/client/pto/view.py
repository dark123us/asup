#!/usr/bin/python
# -*- coding: UTF-8  -*-
from PyQt4 import QtGui, Qt, QtCore
import sys,os
import logging
import time
from datetime import datetime, date
from engine.dictionary.bus import Bus
from engine.dictionary.marka import Marka
from engine.document.planto12 import PlanTo12
from engine.document.readyto12 import ReadyTo12
from engine.journal.to12 import To12
from engine.widget.view import TestWidget

class PtoWindow(QtGui.QMainWindow):
    def __init__(self, auth, parent = None):
        QtGui.QMainWindow.__init__(self, parent)
        self._initvalue(auth)
        self._initwidget()
        self._setcentral()
        self.setWindowTitle(u'АРМ ПТО')
        #self.setWindowState(Qt.Qt.WindowMaximized)
    
    def _initvalue(self, auth):
        self._mdiarea = QtGui.QMdiArea()
        self._mdiarea.setHorizontalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)
        self._mdiarea.setVerticalScrollBarPolicy(QtCore.Qt.ScrollBarAsNeeded)

    def _initmenu(self):
        headermenu = [[u'Файл',
                       [[u'Выход', self.close],
                        ],
                       ],
                      [u'Справочники',
                        [[u'Марки', self._window, Marka],
                         [u'Автобусы', self._window, Bus],
                         ],
                        ],
                      [u'Документы',
                       [[u'Планирование ТО-1, ТО-2', self._docnew, PlanTo12],
                        [u'Проведение ТО-1, ТО-2', self._docnew, ReadyTo12],
                        ],
                       ], 
                      [u'Журналы',
                       [[u'ТО-1 ТО-2',self._journal, To12],
                        ],
                       ],
                      [u'Отчеты',
                       [[u'Тест виджетов', self._testwidget, TestWidget]]
                       ],
                      [u'Окно'],
                      ]
        menu = QtGui.QMenuBar()
        for row in headermenu:
            tmp = menu.addMenu(row[0])
            if len(row) == 2:
                for row2 in row[1]:
                    act = QtGui.QAction(row2[0], self)
                    tmp.addAction(act)
                    if len(row2) == 2:
                        self.connect(act, QtCore.SIGNAL('triggered()'), row2[1])
                    elif len(row2) == 3:
                        self.connect(act, QtCore.SIGNAL('triggered()'), row2[1])
                        act.setData(row2[2])
        self.setMenuBar(menu)
    
    def _initwidget(self):
        self._initmenu()
        self.setCentralWidget(self._mdiarea)
    
    def _setcentral(self):
        screen = QtGui.QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move((screen.width()-size.width())/2,
            (screen.height()-size.height())/2) 
    
    def _window(self):
        obj = self.sender().data().toPyObject()()
        widg = obj.widget()
        wind = QtGui.QMdiSubWindow()
        wind.setWidget(widg)
        wind.setWindowTitle(obj.title())
        self._mdiarea.addSubWindow(wind)
        wind.show()
        widg.redraw()
    
    def _docnew(self):
        obj = self.sender().data().toPyObject()()
        obj.newdocument()
        wind = obj.showdocument()
        self._mdiarea.addSubWindow(wind)
        wind.show()
    
    def _journal(self):
        obj = self.sender().data().toPyObject()(self)
        wind = obj.getwindow()
        self._mdiarea.addSubWindow(wind)
        wind.show()
    
    def _testwidget(self):
        obj = self.sender().data().toPyObject()(self)
        wind = obj#.getwindow()
        self._mdiarea.addSubWindow(wind)
        wind.show()
    
    def setsubwindow(self, wind):
        self._mdiarea.addSubWindow(wind)
        wind.show()

class Pto():
    def start(self, auth):
        logging.debug("start pto component")
        app = QtGui.QApplication(sys.argv)
        mw = PtoWindow(auth)#MainWindow()
        mw.show()
        app.exec_()
        