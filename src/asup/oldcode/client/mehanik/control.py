#!/usr/bin/python
# -*- coding: UTF-8  -*-
from PyQt4 import QtGui, Qt, QtCore
import sys,os
import logging
log = logging.getLogger(__name__)
from models.mehanik import ModelMehanik
from models.printreport import PrintReportSVG
from client.mehanik.view import ViewJornalMehanik, DialogFindBus, ShtampHour
import time
from datetime import datetime, date

class ControlJournalMehanik():
    _model = None
    _view = None
    _timeredraw = 5 #через сколько перерисовывать таблицу - т.е. у модели запрашиваем данные
    _redrawcount = 0 #счетчик срабатываний таймера
    _curdate = None
    _pathtemplate = 'templates'
    
    def __init__(self, model):
        self._model = model #получаем модель Можно и просто ModelMehanik() - т.к. это синглентон
        self._view = ViewJornalMehanik()#получаем вид
        self._model.appendobserver(self)#вешаем прокси наблюдателем в модели
        
        self._dialog = DialogFindBus(self._view)
        self._dialog.setModal(True)
        self._dialogshtamp = QtGui.QDialog(self._view)
        self._dialogshtamp.setModal(True)
        self._dialogshtamp.setMinimumSize(200, 100)
        self._viewshtamp = ShtampHour()
        gr = QtGui.QGridLayout()
        gr.addWidget(self._viewshtamp)
        self._dialogshtamp.setLayout(gr)
        self._initaction()
    
    def getmainview(self):
        return self._view
    
    def _initaction(self):
        self._view.connect(self._view._curdate, QtCore.SIGNAL("dateChanged (QDate)"),self._datechanged)
        self._view.connect(self._view, QtCore.SIGNAL('keypressed(int)'), self._showdialog)
        self._view.connect(self._view, QtCore.SIGNAL('timeout()'), self._timeout)
        self._dialog.connect(self._dialog, QtCore.SIGNAL('accepted()'), self.selected)
        self._viewshtamp.connect(self._viewshtamp, QtCore.SIGNAL("printing(int)"), self.printshtamp)

    def _timeout(self):
        self._redrawcount += 1
        if self._redrawcount > self._timeredraw:
            self._redrawcount = 0
            self._model.updatedata(self._curdate)

    def _datechanged(self, dt):
        self._curdate = dt.toPyDate()
        self._view.changefocus()
        self._model.updatedata(dt.toPyDate())
    
    def modelischanged(self):
        self._view.redraw(self._model.getdata())
    
    def selected(self):
        val = None
        try:
            garnm = int(self._dialog.ed.text())
            val = self._model.getdatabus(garnm)
        except ValueError:
            logging.debug("{0} not int". format(garnm))
        
        if val: #если найден элемент создаем диалоговое окно для отображения
            self._dialogshtamp.setWindowTitle(u'Гаражный {0}'.format(garnm))
            data = self._model.getdatabus(garnm)
            self._viewshtamp.redraw(data)
            self._dialogshtamp.setMinimumSize(800, 350)
            self._dialogshtamp.show()
            
    def _showdialog(self, key):
        self._dialog.ed.setText(chr(key))
        self._dialog.show()
    
    def _savefact(self, row):
        'Сохраняем фактическое время'
        if row == 0 or row == 2:
            self._model.setfactviezd()
        else:
            self._model.setfactvozvr()
    
    def printshtamp(self, row):
        'Печать шаблона'
        self._savefact(row)
        templ = ['mehput.svg','mehput.svg', 'mehhoz.svg','mehhoz.svg']
        dt = ['','','','']
        dt[row] = datetime.now().strftime('%d.%m %H:%M')
        data = {}
        data['timeviezd'] = "{0}{1}".format(dt[0],dt[2])
        data['timevozvr'] = "{0}{1}".format(dt[1],dt[3])
        s = os.sep
        filename  = "{1}{0}{2}{0}{3}".format(s, os.getcwd(),self._pathtemplate,templ[row])
        pr = PrintReportSVG()
        pr.setprinter()
        pr.setdata(data)
        pr.settemplate(filename)
        pr.printing()
        #pr.printpreview()
        logging.debug('send to printer from file {0} data = {1}'.format(filename, data))
        self._dialogshtamp.close()
        
class MehanikWindow(QtGui.QMainWindow):
    def __init__(self, auth, parent = None):
        QtGui.QMainWindow.__init__(self, parent)
        self._initvalue(auth)
        self._initwidget()
        self._setcentral()
        self.setWindowTitle(u'АРМ Механик')
        self.setWindowState(Qt.Qt.WindowMaximized)
    
    def _initvalue(self, auth):
        self._model = ModelMehanik()
        self._controljournal = ControlJournalMehanik(self._model)
        self._timer = QtCore.QTimer()
        self.connect(self._timer, QtCore.SIGNAL('timeout()'), self._firstupdate)
        
        self._dlg = QtGui.QMessageBox(self)
        self._dlg.setWindowTitle(u'Загрузка данных')
        self._dlg.setText(u'''Подождите, пока данные загрузятся''')
        self._dlg.setMinimumSize(200, 100)
        self._dlg.setStandardButtons(QtGui.QMessageBox.Close)
        self._timer.start(100)
        self._dlg.exec_()
    
    def _firstupdate(self):
        self._timer.stop()
        print 1
        self._model.updatedata(date.today())
        print 2
        self._dlg.close()
            
    def _initwidget(self):
        self.setCentralWidget(self._controljournal.getmainview())
    
    def _setcentral(self):
        screen = QtGui.QDesktopWidget().screenGeometry()
        size = self.geometry()
        self.move((screen.width()-size.width())/2,
            (screen.height()-size.height())/2) 
        

class Mehanik():
    def start(self, auth):
        logging.debug("start mehanik component")
        app = QtGui.QApplication(sys.argv)
        mw = MehanikWindow(auth)#MainWindow()
        mw.show()
        app.exec_()