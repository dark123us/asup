# -*- coding: utf-8  -*-'''
#===============================================================================
# Тестовый юнитs        
#===============================================================================
import unittest
import sys
from models.orm import Bus, Marka, Rabot, Putev, PutevKarta, PutevRabot, PutevZakaz, Karta
from PyQt4 import QtGui
from view import ShtampHour 
from datetime import datetime

class TestView(unittest.TestCase):
    _app = None
    @classmethod
    def setUpClass(self):
        self._app = QtGui.QApplication(sys.argv)
        #mw = MehanikWindow(auth)#MainWindow()
        #mw.show()
        #app.exec_()
        
    def test_shtamphour(self):
        sh = ShtampHour()
        self.assertIsInstance(sh, ShtampHour, 'error in create class ShtampHour')
    
    def test_shtamphour_redraw(self):
        r1 = Rabot()
        r1.family = u'Михалков'
        r1.name = u'Василий'
        r1.surname = u'Сергеевич'
        r1.tabnmbr = 1234
        
        r2 = Rabot()
        r2.family = u'Никитин'
        r2.name = u'Андрей'
        r2.surname = u'Васильевич'
        r2.tabnmbr = 4321
        
        marka = Marka()
        marka.name = u'Маз-103'
        
        bus = Bus()
        bus.id = 1
        bus.garnm = 123
        bus.marka = marka
        bus.gosnm = u'A 12-34 ВЕ'
        
        putev1 = Putev()
        putev1.bus = bus
        putev1.dtviezd = datetime(2013,1,1,12,45)
        putev1.dtvozvr = datetime(2013,1,1,17,15)
        putev1.nompl = 23
        putev1.smena = 2
        
        pr1 = PutevRabot()
        pr1.rabot = r1
        pr1.putev = putev1
        
        pr2 = PutevRabot()
        pr2.rabot = r2
        pr2.putev = putev1
        
        karta = Karta()
        karta.name = '1234'
        
        pk = PutevKarta()
        pk.putev = putev1
        pk.karta = karta
    
        data = {}
        data['bus'] = bus
        data['putev'] = {}
        data['putev'][1] = {}
        data['putev'][1]['putev'] = putev1
        data['putev'][1]['karta'] = [pk]
        data['putev'][1]['rabot'] = [pr1, pr2]
        
        sh = ShtampHour()
        sh.redraw(data)
