# -*- coding: utf-8  -*-'''
from PyQt4 import QtGui, QtCore, Qt
from datetime import datetime, date
import time
import logging
log = logging.getLogger(__name__)

class TableJournalMehanik(QtGui.QTableWidget):
    _column = 7 #Количество колонок для журнала
    _data = {} #полученные данные от модели
    _sortkeys = [] #сортировка id по заданному алгоритму
    _view1sm = True
    _view2sm = False
    _curdate = datetime.now()
     
    def __init__(self, parent = None):
        QtGui.QTableWidget.__init__(self, parent)
        self.setEditTriggers(QtGui.QAbstractItemView.EditTriggers(0))
        self.setheader()
        self._backgroundodd = QtGui.QBrush(QtGui.QColor('#ccc'), QtCore.Qt.SolidPattern)
        self._busfont = QtGui.QFont()
        self._busfont.setBold(True)
        self._factfont = QtGui.QFont()
        self._factfont.setBold(True)
        self._factcolor = QtGui.QBrush(QtGui.QColor('#00f'), QtCore.Qt.SolidPattern)
    
    def setcolumn(self, value):
        'Устанавиливаем в сколько колонок отображать данные'
        self._column = value
        self.redraw()
    
    def setheader(self, header = None):
        'Устанавливаем заголовки колонок'
        if not header:
            header = [u'Гаражный']
            if self._view1sm :
                header += [u'1 см\nвыезд', u'1 см\nвозвращ']
            if self._view2sm :
                header += [u'2 см\n выезд', u'2 см\nвозвращ']
    
        headers = []
        for c in range(self._column):
            headers += header
        self.setColumnCount(len(headers))
        self.setHorizontalHeaderLabels(headers)
    
    def _setsortiddata(self, data):
        'Создаем список ключей, отсортированных по небходимому полю'
        list = []
        for key in data.keys():
            list.append([data[key]['bus'].garnm, key])
        list.sort()
        ret = []
        for row in list:
            ret.append(row[1])
        return ret
    
    def redraw(self, data = None):
        'Отрисовка данных'
        self.setheader()
        if data:
            self._data = data
            self._sortkeys = self._setsortiddata(self._data)
        rows = len(self._data) / self._column 
        self.setRowCount(rows+ 1)
        row = 0
        col = 0
        for key in self._sortkeys:
            self._drawrec(row, col, self._data[key])
            row += 1
            if row > rows:
                row = 0
                col += 1
        self.resizeColumnsToContents()
        self.resizeRowsToContents()
    
    def _convdata(self, dt):
        'Возвращаем представление даты в виде строки'
        if dt:
            if self._curdate == dt.date():
                return dt.strftime("%H:%M")
            else:
                return dt.strftime("%d.%m %H:%M")
        else:
            return ""
    
    def setview1sm(self, value):
        self._view1sm = value
        self.redraw()
    
    def setview2sm(self, value):
        self._view2sm = value
        self.redraw()
    
    def setcurdate(self, dt):
        self._curdate = dt
    
    def _drawpalnfact(self, row, column, rec):
        'Отрисовываем план и факт'
        inc = 1
        if self._view1sm:
            for i in range(2):
                try:
                    fact = self._convdata(rec['fact'][i])
                except:
                    from pprint import pprint 
                    pprint (rec)
                    import sys
                    sys.exit()
                    
                if fact: 
                    self._drawitem(row, column + inc, fact, 
                                   styleforeground = self._factcolor, 
                                   font = self._factfont)
                else:
                    self._drawitem(row, column + inc, self._convdata(rec['plan'][i]))
                inc += 1
        if self._view2sm:
            for i in range(2,4):
                fact = self._convdata(rec['fact'][i])
                if fact: 
                    self._drawitem(row, column + inc, fact, 
                                   styleforeground = self._factcolor, 
                                   font = self._factfont)
                else:
                    self._drawitem(row, column + inc, self._convdata(rec['plan'][i]))
                inc += 1

    def _drawrec(self, row, col, rec):
        'Отрисовываем один элемент - может состоять из нескольких колонок'
        add = 1
        if self._view1sm:
            add += 2
        if self._view2sm:
            add += 2
        column = col * add
        background = None
        if 'shkiper' in rec:
            if rec['shkiper']['true'] == 0 and rec['shkiper']['false'] == 0:
                background = QtGui.QBrush(QtGui.QColor('#a00'), QtCore.Qt.SolidPattern)
            elif rec['shkiper']['true'] == 0 :
                background = QtGui.QBrush(QtGui.QColor('#aa0'), QtCore.Qt.SolidPattern)
            
        self._drawitem(row, column, str(rec['bus'].garnm), font =  self._busfont, background = background)
        self._drawpalnfact(row, column, rec)
    
    def _drawitem(self, row, column, msg, background = None,
                   styleforeground = None, font = None):
        item = QtGui.QTableWidgetItem()
        if background is None:
            if not row & 1:
                item.setBackground(self._backgroundodd)
        else:
            item.setBackground(background)
        if font:
            item.setFont(font)
        if styleforeground:
            item.setForeground(styleforeground)
        item.setText(msg)
        self.setItem(row, column, item)
        
        
class ViewJornalMehanik(QtGui.QWidget):
    def __init__(self, parent = None):
        QtGui.QWidget.__init__(self, parent)
        self._initvalue()
        self._initwidget()
        
    
    def _initvalue(self):
        self._timer = QtCore.QTimer()
        self._clock = QtGui.QLabel(u'00:00:00')
        self._curdate = QtGui.QDateEdit()
        self._curdate.setDate(QtCore.QDate(datetime.now()))
        self._curdate.setCalendarPopup(True)
        f = QtGui.QFont()
        f.setPointSize(40)
        self._clock.setFont(f)
        self._chsm1 = QtGui.QCheckBox(u'1 смена')
        self._chsm1.setCheckState(2)
        self._chsm2 = QtGui.QCheckBox(u'2 смена')
        self._chsm2.setCheckState(0)
        self._journal = TableJournalMehanik()
        
    def _initwidget(self):
        v = QtGui.QVBoxLayout()
        v.addWidget(self._chsm1)
        v.addWidget(self._chsm2)
        
        v2 = QtGui.QVBoxLayout()
        v2.addWidget(self._curdate)
        
        h = QtGui.QHBoxLayout()
        h.addWidget(self._clock)
        h.addLayout(v)
        h.addLayout(v2)
        
        h.addStretch(1)
        gr = QtGui.QGridLayout()
        gr.addLayout(h,0,0)
        gr.addWidget(self._journal)
        self.setLayout(gr)
        self.connect(self._chsm1, QtCore.SIGNAL("stateChanged(int)"), self._setchecksm1)
        self.connect(self._chsm2, QtCore.SIGNAL("stateChanged(int)"), self._setchecksm2)
        self.connect(self._timer, QtCore.SIGNAL("timeout()"), self._timeout)
        self.connect(self, QtCore.SIGNAL("closewidget()"), self._stoptimer)
        self._timer.start(1000)
    
    def setcurdate(self, dt):
        self._curdate.setDate(QtCore.QDate(dt))
    
    def _timeout(self):
        'Сработка таймера - устанавливаем текущее время'
        tm = time.localtime()
        self._settime(tm[3], tm[4], tm[5])
        self.emit(QtCore.SIGNAL('timeout()'))
    
    def _stoptimer(self):
        'остановка таймера'
        self._view._timer.stop()

    def _setchecksm1(self, state):
        'Клик по 1-смене'
        if state == 0 and self._chsm2.checkState() == 0:
            self._chsm1.setCheckState(2)
            self._journal.setview1sm(True)
            self._journal.setview2sm(False)
        else:
            if state:
                self._journal.setview1sm(True)
            else:
                self._journal.setview1sm(False)
            
    def _setchecksm2(self, state):
        'Клик по 2-смене'
        if state == 0 and self._chsm1.checkState() == 0:
            self._chsm1.setCheckState(2)
            self._journal.setview1sm(True)
            self._journal.setview2sm(False)
        else:
            if state:
                self._journal.setview2sm(True)
            else:
                self._journal.setview2sm(False)
    
    def _settime(self, hour, minute, second):
        'Установить время (текущее)'
        self._clock.setText('%02d:%02d:%02d'%(hour, minute, second))
    
    def redraw(self, data):
        self._journal.setcurdate(self._curdate.date().toPyDate())
        self._journal.redraw(data)
    
    def keyReleaseEvent (self, event):
        'Отлавливаем клавиши'
        if event.key() > 47 and event.key()<58:
            self.emit(QtCore.SIGNAL('keypressed(int)'),event.key())
        QtGui.QWidget.keyReleaseEvent(self, event)
    
    def changefocus(self):
        self._journal.setFocus()

#===============================================================================
# Диалог длля поиска по гаражному
#===============================================================================
class DialogFindBus(QtGui.QDialog):
    'Быстрый поиск автобуса по гаражному'
    def __init__(self, parent = None):
        'Переопределяем для снятия выделения и инициализации переменных'
        QtGui.QDialog.__init__(self, parent)
        self.setWindowTitle(u'Поиск по гаражному')
        self.ed = QtGui.QLineEdit()
        self.dbutton = QtGui.QDialogButtonBox(QtGui.QDialogButtonBox.Ok|QtGui.QDialogButtonBox.Cancel)
        self.connect(self.dbutton, QtCore.SIGNAL("rejected()"),self.reject) 
        self.connect(self.dbutton, QtCore.SIGNAL("accepted()"),self.accept)
        self.timer = QtCore.QTimer()
        self.connect(self.timer, QtCore.SIGNAL("timeout()"), self.deselect)
        gr = QtGui.QGridLayout()
        gr.addWidget(self.ed)
        gr.addWidget(self.dbutton)
        self.setLayout(gr)
    
    def show(self, *args, **kwargs):
        'Переопределяем для старта таймера, чтобы снять выделение'
        self.timer.start(10)
        self.ed.setFocus()
        return QtGui.QDialog.show(self, *args, **kwargs)
    
    def deselect(self):
        'Стоп таймера снимаем выделение'
        self.timer.stop()
        self.ed.deselect()        
#===============================================================================
# Виджет для диалога для работы с гаражным
#===============================================================================

class ShtampTable(QtGui.QTableWidget):
    'Таблица данных штамп часов'        
    def __init__(self, parent = None):
        QtGui.QTableWidget.__init__(self, parent)
        self.verticalHeader().setVisible(False)
        self.horizontalHeader().setVisible(False)
        self.setEditTriggers(QtGui.QAbstractItemView.EditTriggers(0))
        self.connect(self, QtCore.SIGNAL('cellClicked(int ,int )'), self._sendprintsignal)
        self.redraw()

    'QTableWidget переопределяем keyPressEvent - ловим нажатие ентеров'
    def keyPressEvent(self, event):
        'Переопределяем реакцию на клавиши для ловли ентеров'
        if event.key() == QtCore.Qt.Key_Return:
            self._sendprintsignal(self.currentRow(), 0)
        if event.key() == QtCore.Qt.Key_Enter:
            self._sendprintsignal(self.currentRow(), 0)
        QtGui.QTableWidget.keyPressEvent(self,event)
    
    def _sendprintsignal(self, row, col):
        self.emit(QtCore.SIGNAL("printing(int)"),row)    
    
    def redraw(self):
        st = [u'Выезд - Основное',
               u'Возвращение - Основное',
               u'Выезд - Хозпарк',
               u'Возвращение - Хозпарк']
        self.setRowCount(len(st))
        self.setColumnCount(1)
        
        fnt = QtGui.QFont()
        fnt.setPixelSize(30)
        
        for ind, s in enumerate(st):
            item = QtGui.QTableWidgetItem()
            item.setFont(fnt)
            item.setText(u"{0}".format(s))
            self.setItem(ind,0, item)
        self.resizeColumnsToContents()
        self.resizeRowsToContents()
        self.setMinimumWidth(self.columnWidth(0))
            
class ShtampHourPutev(QtGui.QWidget):
    def __init__(self, parent = None):
        QtGui.QWidget.__init__(self, parent)
        self._dtnone = "--.-- --:--"
        self._initwidget()
    
    def _initwidget(self):
        putev = QtGui.QLabel(u'Путевой лист')
        self._putev = QtGui.QLabel(u'№ 000001')
        
        hl1 = QtGui.QHBoxLayout()
        hl1.addWidget(putev)
        hl1.addWidget(self._putev)
        
        self._gbdr = QtGui.QGroupBox(u'Водители')
        self._vboxdr = QtGui.QVBoxLayout()
        self._gbdr.setLayout(self._vboxdr)
        
        self._vboxdr.addWidget(QtGui.QLabel('Driver 1'))
        self._vboxdr.addWidget(QtGui.QLabel('Driver 2'))
        
        karta = QtGui.QLabel(u'Карточка')
        self._karta = QtGui.QLabel(u'12312')
        hl2 = QtGui.QHBoxLayout()
        hl2.addWidget(karta)
        hl2.addWidget(self._karta)
        
        tmbox = QtGui.QGroupBox(u'Время')
        htmbox = QtGui.QHBoxLayout()
        tmbox.setLayout(htmbox)
        
        tmboxviezd = QtGui.QGroupBox(u'выезда')
        #vtmboxviezd = QtGui.QVBoxLayout()
        #tmboxviezd.setLayout(vtmboxviezd)
        grtm1 = QtGui.QGridLayout()
        tmviezdplan = QtGui.QLabel(u'План')
        tmviezdfact = QtGui.QLabel(u'Факт')
        self._tmviezdplan = QtGui.QLabel(self._dtnone)
        self._tmviezdfact = QtGui.QLabel(self._dtnone)
        grtm1.addWidget(tmviezdplan, 0, 0)
        grtm1.addWidget(self._tmviezdplan, 0, 1)
        grtm1.addWidget(tmviezdfact, 1, 0)
        grtm1.addWidget(self._tmviezdfact, 1, 1)
        #vtmboxviezd.addLayout(grtm1)
        tmboxviezd.setLayout(grtm1)
        
        tmboxvozvr = QtGui.QGroupBox(u'возвращения')
        vtmboxvozvr = QtGui.QVBoxLayout()
        tmboxvozvr.setLayout(vtmboxvozvr)
        self._tmvozvrplan = QtGui.QLabel(self._dtnone)
        self._tmvozvrfact = QtGui.QLabel(self._dtnone)
        vtmboxvozvr.addWidget(self._tmvozvrplan)
        vtmboxvozvr.addWidget(self._tmvozvrfact)
        
        htmbox.addWidget(tmboxviezd)
        htmbox.addWidget(tmboxvozvr)
        
        gr = QtGui.QGridLayout()
        gr.addLayout(hl1,0,0)
        gr.addWidget(self._gbdr)
        gr.addLayout(hl2,2,0)
        gr.addWidget(tmbox)
        
        self.setLayout(gr)
    
    def _getstrfromdt(self, dt):
        'Возвращаем представление даты в виде строки'
        if dt:
            return dt.strftime("%d.%m %H:%M")
        else:
            return self._dtnone
    
    def _redrawputev(self, data):
        nput = data['putev'].nompl
        dtviezd = self._getstrfromdt(data['putev'].dtviezd)
        dtvozvr = self._getstrfromdt(data['putev'].dtvozvr)
        dtfactviezd = self._getstrfromdt(data['putev'].dtfactviezd)
        dtfactvozvr = self._getstrfromdt(data['putev'].dtfactvozvr)
        
        self._putev.setText(u"№ {0}".format(nput))
        self._tmviezdplan.setText("{0}".format(dtviezd))
        self._tmvozvrplan.setText("{0}".format(dtvozvr))
        self._tmviezdfact.setText("{0}".format(dtfactviezd))
        self._tmvozvrfact.setText("{0}".format(dtfactvozvr))
    
    def _redrawkarta(self, data):
        st = ''
        add = ''
        if 'karta' in data.keys():
            for pk in data['karta']:
                try:
                    st = u"{0}{1}{2}".format(st, add)
                    st = u"{0}{1}{2}".format(st, add, pk.karta.name)
                except Exception, e:
                    log.error('{0}{1}'.format(type(e), e))
                add = ', '
        self._karta.setText(st)
    
    def _redrawrabot(self, data):
        while (not self._vboxdr.isEmpty()):
            widg = self._vboxdr.takeAt(0)
            #self._vboxdr.removeItem(widg)
            if widg:
                widg.widget().setVisible(False)
                del widg
        if 'rabot' in data.keys():
            for pr in data['rabot']:
                s = ''
                n = ''
                fio = 'unknown'
                try:
                    s = u"{0}.".format(pr.rabot.surname[0])
                except Exception, e:
                    log.error('{0}{1}'.format(type(e), e))
                try:
                    n = u"{0}.".format(pr.rabot.name[0])
                except Exception, e:
                    log.error('{0}{1}'.format(type(e), e))
                try:
                    fio = u"{0} {1}{2}".format(pr.rabot.family, n, s)
                except Exception, e:
                    log.error('{0}{1}'.format(type(e), e))
                rabot = QtGui.QLabel(fio)
                self._vboxdr.addWidget(rabot)
        
    def redraw(self, data):
        self._redrawputev(data)
        self._redrawkarta(data)
        self._redrawrabot(data)

class ShtampHour(QtGui.QWidget):
    'Виджет диалога штамп часов'
    def __init__(self, parent = None):
        QtGui.QWidget.__init__(self, parent)
        self._initwidget()
    
    def _initwidget(self):
        lblgar = QtGui.QLabel(u'Гаражный номер')
        self._lblerrorshkiper = QtGui.QLabel(u'Нет сигнала от Шкипер-01Е')
        self._lblerrorshkiper.setStyleSheet("QLabel { background-color : red; color : white; font: bold 20px; }")
        #self._lblerrorshkiper.setVisible(False)
        
        self._lblgar = QtGui.QLabel(u'123')
        hl1 = QtGui.QHBoxLayout()
        hl1.addWidget(lblgar)
        hl1.addWidget(self._lblgar)

        self._put = []
        self._gbsm = []
        hl2 = QtGui.QHBoxLayout()
        for i in (1,2):
            tmp = ShtampHourPutev()
            tmpgb = QtGui.QGroupBox(u'{0} смена'.format(i)) 

            vl1 = QtGui.QVBoxLayout()
            vl1.addWidget(tmp)
            tmpgb.setLayout(vl1)
            tmpgb.setVisible(False)
            self._gbsm.append(tmpgb)
            self._put.append(tmp)
            hl2.addWidget(tmpgb)
        
        self._shtamp = ShtampTable()
        self.connect(self._shtamp, QtCore.SIGNAL('printing(int)'), self._printing)
                     
        gr = QtGui.QGridLayout()
        vl2 = QtGui.QVBoxLayout()
        vl2.addLayout(hl1)
        vl2.addWidget(self._lblerrorshkiper)
        gr.addLayout(vl2,0,0)
        gr.addLayout(hl2,1,0)
        gr.addWidget(self._shtamp,1,2)
        self.setLayout(gr)

#     def getputevobject(self):
#         'Возвращаем объекты putev, чтобы можно было определить, куда воткнуть фактическое'
#         data = []
#         for row in self._put:
#             data = row.
#         return data

    def _printing(self, row):
        self.emit(QtCore.SIGNAL("printing(int)"),row)    
    
    def _redrawshkiper(self, data):
        self._lblerrorshkiper.setVisible(False)
        if 'shkiper' in data:
            if data['shkiper']['true'] == 0 and data['shkiper']['false'] == 0:
                self._lblerrorshkiper.setText(u'Нет сигнала от Шкипер-01Е')
                self._lblerrorshkiper.setStyleSheet("QLabel { background-color : red; color : white; font: bold 26px; }")
                self._lblerrorshkiper.setVisible(True)
            elif data['shkiper']['true'] == 0 :
                self._lblerrorshkiper.setText(u'Неисправна антенна Шкипер-01Е')
                self._lblerrorshkiper.setStyleSheet("QLabel { background-color : yellow; color : black; font: bold 26px; }")
                self._lblerrorshkiper.setVisible(True)
                
            
    
    def redraw(self, data):
        'Отрисовка полученных данных'
        self._redrawshkiper(data)
        self._lblgar.setText(u"{0} [г.н.{1}] [{2}]".format(data["bus"].garnm,
                                              data["bus"].gosnm,
                                              data["bus"].marka.name))
        for i in (1,2):
            if i in data['putev'].keys():
                self._put[i-1].redraw(data['putev'][i])
                self._gbsm[i-1].setVisible(True)
            else:
                self._gbsm[i-1].setVisible(False)
                
                
        
    
    
        

        