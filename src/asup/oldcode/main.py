#!/usr/bin/python
# -*- coding: utf-8  -*-
from utils.update import Update
from engine.selectcomponent import SelectComponent
from engine.log import Log
from server.authorize import ClientAuthorize
import subprocess
import sys
import logging
import argparse

Log()
log = logging.getLogger(__name__)


def update():
    'Выполнение обновления программы'
    upd = Update()
    upd._path = ''
    return upd.update()

def restart():
    'Перезапуск программы'
    subprocess.check_output('python main.py', shell=True)
    sys.exit(0)

def authorize(component):
    'Авторизация, возвращаем либо None - не пройдена, либо данные для компонента'
    return ClientAuthorize().start()

def main(componentid = -1):
    log.debug('Begin logging')
    try:
        if update():
            restart()
        component = SelectComponent().getcomponent(componentid)
        log.debug("{0}".format(component))
        if component:
            if component.needauthorize():
                auth = authorize(component)
                if auth:
                    component.start(auth)
            else:
                component.start()
    except Exception, e:        
        log.fatal('{0} {1}'.format(type(e), e))
        raise
    log.debug('End logging')
    log.debug('='*40)
    
if __name__ == "__main__":
    if len(sys.argv)>1:
        parser = argparse.ArgumentParser(description='Main unit for run anything components from console')
        #parser = argparse.ArgumentParser()
        parser.add_argument("-c", "--component", action="store",
                            help="component using for working (authorize, client)",
                            type = str)
        args = parser.parse_args()
        main(args.component)
    else:
        main()