#!/usr/bin/python
# -*- coding: utf-8  -*-'''

'''Обновление программы, используем в конфиге значения, с какой веткой будем
обновляться'''
import os
from configobj import ConfigObj 
import subprocess
import logging

class Update():
    '''Класс работы обновления.'''
    
    _path = '/..'
    def update(self):
        '''Автоматическое обновление, если изменилось в репе, возвращаем True и обновляемся иначе False -
        не автоматическое обновление или ничего не изменилось, идентичен git pull origin master, где 
        origin master получаем из общего конфига::
                
                [UPDATE]
                method = auto / manual
                server = origin
                branch = master
                
        Для работы репозитарий необходимо склонировать, тогда в гите будет путь
        к клону, с кем сравнивать
        '''
        configPath = os.getcwd() + self._path + '/config.ini'
        config = ConfigObj(configPath)

        method = config['UPDATE']['method']
        ret = False
        if method == 'auto':
            server = config['UPDATE']['server']
            branch = config['UPDATE']['branch']
            sourceOutput = subprocess.check_output('git pull {0} {1}'.format(server, branch), shell=True)
            logging.debug("pull return message:\n{0}".format(sourceOutput))
            #print (sourceOutput,)
            if not(sourceOutput in 'Already up-to-date.\n'):
                ret = True
        return ret 

def main():
    update = Update()
    return update.update()

if __name__ == '__main__':
    'Пример перезапуска программы в случае обновления'
    if main():
        print "="*20
        print "System updateted, restart"
        subprocess.check_output('python update.py', shell=True)
