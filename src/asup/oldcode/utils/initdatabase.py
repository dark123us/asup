# -*- coding: utf-8  -*-
from models.orm import DBSession, Base
from sqlalchemy import create_engine

class InitDataBase():
    def initdatabase(self, engine):
        DBSession.configure(bind=engine)
        try:
            Base.metadata.create_all(engine)
        except Exception, e:
            print "{0}{1}".format(type(e),e)
            raise
            

if __name__ == "__main__":
    engine = 'mysql+mysqldb'
    user = 'asuptest'
    passwd = '12345'
    host = '10.1.12.52'
    dbname = 'asuptest2'
    engine = create_engine('{0}://{1}:{2}@{3}/{4}?charset=utf8'.format(engine,user,passwd, host,dbname))
    i  = InitDataBase()
    i.initdatabase(engine)