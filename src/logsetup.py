import logging.config
logging.config
logging.basicConfig(filename='../logs/test.log',
                    filemode='w',
                    level=logging.DEBUG,
                    # format = '[%(asctime)s]# %(levelname)-8s %(threadName)-13s'
                    # ' %(name)-40s %(message)-100s [%(funcName)s:LINE:%(lineno)d]',
                    format = '[%(asctime)s]# %(levelname)-8s '
                    ' %(name)-40s %(message)-100s [%(funcName)s:LINE:%(lineno)d]',
                    )
